# Copyright (C) 2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import bpy
from bpy.props import CollectionProperty, StringProperty, BoolProperty
import os
from os import listdir
from os.path import isdir, join, isfile
import re
from bpy.app.handlers import persistent
from math import pi

from .utils import assign_object_to_collection


class LFSSceneBuilderImportSet(bpy.types.Operator):
    """Import set from asset server"""
    bl_idname = "pipeline.scene_builder_import_set"
    bl_label = "Import Set"
    bl_options = {'REGISTER', 'UNDO'}

    # File props
    files: CollectionProperty(type=bpy.types.OperatorFileListElement,
                              options={'HIDDEN', 'SKIP_SAVE'})
    directory: StringProperty(maxlen=1024, subtype='FILE_PATH',
                              options={'HIDDEN', 'SKIP_SAVE'})

    filter_folder: BoolProperty(default=True, options={'HIDDEN', 'SKIP_SAVE'})
    filter_image: BoolProperty(default=True, options={'HIDDEN', 'SKIP_SAVE'})

    def invoke(self, context, _event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        # Create projection camera
        cam = bpy.data.cameras.new("Projection_camera")
        projection_camera = bpy.data.objects.new("Projection_camera", cam)
        assign_object_to_collection(projection_camera, "Sets")

        if context.scene.camera is not None and context.scene.camera.type == 'CAMERA':
            projection_camera.data.lens = context.scene.camera.data.lens
            projection_camera.matrix_world = context.scene.camera.matrix_world
        else:
            projection_camera.location = (0.0, -10.0, 1.5)
            projection_camera.rotation_euler = (pi/2.0, 0.0, 0.0)

        # If scene contains a 2D camera rig, create constraint on it to this camera
        if context.scene.camera is not None and context.scene.camera.type == 'CAMERA':
            scene_cam = context.scene.camera
            if (scene_cam.parent is not None
                    and scene_cam.parent.type == 'ARMATURE'
                    and 'Root' in scene_cam.parent.pose.bones):
                con = scene_cam.parent.pose.bones['Root'].constraints.new("COPY_TRANSFORMS")
                con.target = projection_camera

        for obj in context.scene.objects:
            obj.select_set(False)
        projection_camera.select_set(True)
        context.view_layer.objects.active = projection_camera

        # Import planes
        files = [{"name": f.name} for f in self.files]

        bpy.ops.camera.camera_plane_build(directory=self.directory, files=files, step=1.0)

        # Make sets not cast any shadow
        for plane in projection_camera.children:
            plane.active_material.shadow_method = 'NONE'

        return {'FINISHED'}


########################################################################
#                            Set updater
########################################################################

to_be_updated_data = []
to_be_updated_visual = []
to_be_added = []
to_be_removed_visual = []
to_be_removed_data = []
keys = {}

update_status = "UNDEFINED"

########################################################################

def get_textures_from(plane):
    images_selected = []

    for slot in plane.material_slots:
        mat = slot.material
        if mat.node_tree is not None:
            for node in mat.node_tree.nodes:
                if node.type == "TEX_IMAGE" and node.image is not None:
                    images_selected.append(node.image)

    if (len(images_selected) > 1):
        return {
            "warning": True,
            "data": images_selected,
            "Reason": "Too many textures assigned to the plane"
        }
    elif (len(images_selected) == 0):
        return {
            "warning": True,
            "data": None,
            "Reason": "No texture found on plane"
        }
    else:
        return {"warning": False, "data": images_selected[0], "Reason": "OK"}


# Recognizes the name of a file according both the old and the new naming convention
regex = "(lib)?.*(sq[0-9]{2}bg[0-9]{4}).*(_[0-9]{2,4}_)(.+\.png)"

########################################################################

def convert_format(expr, reg):
    '''Takes a file name as argument. Whether it was following the old or the new
    naming convention, returns the name according the new naming convention
    '''
    res = re.search(reg, expr)
    if (res is not None):
        built = (res.group(2) + res.group(3) + res.group(4))
        return built
    else:
        print("WARNING: " + expr
              + " Unexpected file in the directory. File skipped")
        return ""

########################################################################

def version_string(index):
    if (not isinstance(index, int)):
        raise NameError('Version number should be an integer')
    return "v{:03}".format(index)

########################################################################

def directories_list(planes):
    global keys

    # Extraction of images already present, and their location on the disk.
    for plane in planes:
        # Returns an object containing a bpy.types.Image and an error report
        tex_and_status = get_textures_from(plane)

        if (tex_and_status["warning"]):
            print("WARNING", tex_and_status["Reason"], "Plane skipped.")
        else:
            path = tex_and_status["data"].filepath
            tail = os.path.dirname(path)  # Directory path
            head = os.path.basename(path)  # File name
            version = int(os.path.basename(tail)[1:])
            abso_path = os.path.abspath(
                bpy.path.abspath(os.path.join(tail, os.pardir)))

            # Directory as keys of the dictionary
            if (abso_path not in keys):
                keys[abso_path] = []

            # Mendatory to save the path if some planes can come from other directories
            new_file = {
                "name": convert_format(head, regex),
                "actual_name": head,
                "version": version,
                "data": tex_and_status["data"],
                "status": -1,
                "object": plane
            }

            if len(new_file['name']) > 0:
                keys[abso_path].append(new_file)

########################################################################

def detect_updates():
    global to_be_updated_data   # Data-block of images
    global to_be_updated_visual # Informations required for update (path, versions, ...)
    global to_be_added          # Detected as being able to be added
    global to_be_removed_visual # Not detected in the directory anymore
    global to_be_removed_data
    global keys   # Global data about files, before it was sorted in a category (update, addition, deletion)

    # Contains the latest versions of all "included" directories
    to_explore = []

    for path in keys:
        # List of directories representing versions
        directories = [
            join(path, d) for d in listdir(path)
            if isdir(join(path, d)) and re.match("^v[0-9]+$", d)
        ]
        directories.sort()  # Ordered according to version number
        to_explore.append(directories[-1])  # We keep only the latest

    content = {}
    # Fetching content and version of all concerned files from desired directories
    for path in to_explore:
        version = int(os.path.basename(path)[1:])
        path_key = os.path.abspath(
            bpy.path.abspath(os.path.join(path, os.pardir)))
        images = [{
            "name": convert_format(d, regex),
            "actual_name": d,
            "version": version,
            "status": 1}
                  for d in listdir(path)
                  if (isfile(join(path, d))
                      and not d.endswith(".zip"))]
        for i, img in enumerate(images):
            if len(img["name"]) == 0:
                del images[i]
        content[path_key] = images

    # Status:
    #  -1: Has to be removed from file
    #   1: Has to be added to file
    #   2: Has to be updated
    #   0: Nothing to do

    for key_disk in content:  # For each last version of directory required to perform the update
        for img_disk in content[key_disk]:  # For each image of the file
            # Try to find if this image is already included in the scene
            for key_file in keys:
                matching = False
                for img_file in keys[key_file]:
                    if (key_disk != key_file):
                        # We don't need to compare files from two different directories
                        continue
                    else:
                        img_disk_splitted = img_disk["name"].split('_')
                        img_disk_splitted[1] = f'{int(img_disk_splitted[1]):03}'
                        img_disk_name = "_".join(img_disk_splitted[1:])

                        img_file_splitted = img_file["name"].split('_')
                        img_file_splitted[1] = f'{int(img_file_splitted[1]):03}'
                        img_file_name = "_".join(img_file_splitted[1:])
                        
                        if (img_disk_name == img_file_name
                                and len(img_disk["name"]) > 0):
                            img_disk["status"] = 0
                            img_file["status"] = 0
                            if (img_disk["version"] > img_file["version"]):
                                img_disk["status"] = 2
                                img_file["status"] = 2
                                to_be_updated_data.append(img_file["data"])
                                true_path = os.path.join(
                                    key_disk,
                                    version_string(img_disk["version"]))
                                true_path = os.path.join(
                                    true_path, img_disk["actual_name"])
                                to_be_updated_visual.append({
                                    "name":
                                    img_disk["name"],
                                    "old_version":
                                    img_file["version"],
                                    "new_version":
                                    img_disk["version"],
                                    "path":
                                    true_path
                                })

    # Detection of deletions
    for path_f in keys:
        for img in keys[path_f]:
            if img["status"] == -1:
                to_be_removed_visual.append(img["name"])
                to_be_removed_data.append((img["data"], img["object"]))

    # Detection of additions
    for path_d in content:
        for img in content[path_d]:
            if img["status"] == 1:
                true_path = os.path.join(path_d,
                                         version_string(img["version"]))
                true_path = os.path.join(true_path, img["actual_name"])
                to_be_added.append((img["name"], true_path))

########################################################################

def additions_planes():
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = bpy.data.objects['Projection_camera']
    bpy.data.objects['Projection_camera'].select_set(True)

    dir_and_names = {}
    planes_ori = set(bpy.context.scene.collection.children["Sets"].
                     objects["Projection_camera"].children)

    for item in bpy.context.scene.set_updater.additions_planes:
        if item.action == 'add':
            from_dir = os.path.dirname(item.path)
            file = os.path.basename(item.path)
            if from_dir not in dir_and_names:
                dir_and_names[from_dir] = []

            dir_and_names[from_dir].append({'name': file})

    for dir, files in dir_and_names.items():
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects["Projection_camera"].select_set(True)
        bpy.context.view_layer.objects.active = bpy.data.objects["Projection_camera"]
        bpy.context.view_layer.update()
        bpy.ops.camera.camera_plane_build(directory=dir, files=files)

    planes_after = set(bpy.context.scene.collection.children["Sets"].objects["Projection_camera"].children)
    added = planes_after - planes_ori

    offset = 1.0
    for plane in added:
        plane.camera_plane_distance = offset
        offset += 0.4

########################################################################

def deletions_planes():
    global to_be_removed_data
    for item in bpy.context.scene.set_updater.deletions_planes:
        if (item.action == 'remove'):
            bpy.data.objects.remove(to_be_removed_data[item.index][1])
            bpy.data.images.remove(to_be_removed_data[item.index][0])

########################################################################

def updates_planes():
    global to_be_updated_data
    for item in bpy.context.scene.set_updater.updates_planes:
        if (item.action == 'update'):
            to_be_updated_data[item.index].filepath = bpy.path.relpath(item.path)
            to_be_updated_data[item.index].reload()

########################################################################

def apply_updates():
    global update_status
    global to_be_updated_data
    global to_be_updated_visual
    global to_be_added
    global to_be_removed_visual
    global to_be_removed_data
    global keys

    additions_planes()
    deletions_planes()
    updates_planes()

    to_be_updated_data.clear()
    to_be_updated_visual.clear()
    to_be_added.clear()
    to_be_removed_visual.clear()
    to_be_removed_data.clear()
    keys.clear()

    bpy.context.scene.set_updater.additions_planes.clear()
    bpy.context.scene.set_updater.deletions_planes.clear()
    bpy.context.scene.set_updater.updates_planes.clear()

    update_status = 'UNDEFINED'

########################################################################

class ApplyUpdatesOperator(bpy.types.Operator):
    """Imports new versions according the user's selection"""
    bl_idname = "pipeline.apply_planes_updates"
    bl_label = "Apply updates"
    options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def execute(self, context):
        apply_updates()
        self.report({'INFO'}, "Scene planes up to date")
        return {'FINISHED'}

########################################################################
# (unique identifier, property name, property description, icon identifier, number)

class UpdatePlanesData(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(default="")

    old_version: bpy.props.IntProperty(default=0)

    new_version: bpy.props.IntProperty(default=0)

    action: bpy.props.EnumProperty(
        items=[('update', '', 'Update this item', 'FILE_TICK', 0),
               ('skip', '', 'Keep current version', 'RADIOBUT_OFF', 1)
        ],
        default='update')

    path: bpy.props.StringProperty(default="")

    index: bpy.props.IntProperty(default=0)

    def set(self, dico, id):
        self.name = dico['name']
        self.old_version = dico['old_version']
        self.new_version = dico['new_version']
        self.path = dico['path']
        self.index = id

########################################################################

class AdditionPlanesData(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(default="")

    action: bpy.props.EnumProperty(
        items=[('add', '', 'Add this item', 'FILE_NEW', 0),
               ('skip', '', 'Keep current version', 'RADIOBUT_OFF', 1)
        ],
        default='skip')

    path: bpy.props.StringProperty()

    def set(self, tpl):
        self.name = tpl[0]
        self.path = tpl[1]


########################################################################

class DeletionPlanesData(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(default="")

    action: bpy.props.EnumProperty(
        items=[('remove', '', 'Remove this item', 'TRASH', 0),
               ('skip', '', 'Keep current version', 'RADIOBUT_OFF', 1)
        ],
        default='skip')

    index: bpy.props.IntProperty(default=0)

    def set(self, _name, id):
        self.name = _name
        self.index = id

########################################################################

class PipelineData(bpy.types.PropertyGroup):
    additions_planes: bpy.props.CollectionProperty(type=AdditionPlanesData)
    deletions_planes: bpy.props.CollectionProperty(type=DeletionPlanesData)
    updates_planes: bpy.props.CollectionProperty(type=UpdatePlanesData)

########################################################################

class UpdatesChecker(bpy.types.Operator):
    """Checks for update through all required directories"""
    bl_idname = "pipeline.check_updates"
    bl_label = "Check for updates"

    @classmethod
    def poll(cls, context):
        if context.mode != 'OBJECT':
            return False
        if not "Sets" in bpy.context.scene.collection.children:
            return False

        planes = [obj for obj in bpy.context.scene.collection.children["Sets"].all_objects
                  if 'camera_plane_distance' in obj]

        return len(planes)

    def execute(self, context):
        global keys
        global update_status

        bpy.context.scene.set_updater.additions_planes.clear()
        bpy.context.scene.set_updater.deletions_planes.clear()
        bpy.context.scene.set_updater.updates_planes.clear()

        # List of planes
        planes = [obj for obj in bpy.context.scene.collection.children["Sets"].all_objects
                  if 'camera_plane_distance' in obj]

        directories_list(planes)

        detect_updates()

        for id, data in enumerate(to_be_updated_visual):
            bpy.context.scene.set_updater.updates_planes.add().set(data, id)

        for id, data in enumerate(to_be_removed_visual):
            bpy.context.scene.set_updater.deletions_planes.add().set(data, id)

        for data in to_be_added:
            bpy.context.scene.set_updater.additions_planes.add().set(data)

        if (len(bpy.context.scene.set_updater.updates_planes)
                or len(bpy.context.scene.set_updater.deletions_planes)
                or len(bpy.context.scene.set_updater.additions_planes)):
            update_status = "READY"
        else:
            update_status = "NOTHING"

        return {'FINISHED'}

########################################################################

class CancelUpdater(bpy.types.Operator):
    """Resets the checking of updates"""
    bl_idname = "pipeline.updater_cancel"
    bl_label = "Cancel"

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def execute(self, context):
        global update_status
        global keys
        global to_be_updated_data
        global to_be_updated_visual
        global to_be_added
        global to_be_removed_visual
        global to_be_removed_data

        to_be_updated_data.clear()
        to_be_updated_visual.clear()
        to_be_added.clear()
        to_be_removed_visual.clear()
        to_be_removed_data.clear()
        keys.clear()

        bpy.context.scene.set_updater.additions_planes.clear()
        bpy.context.scene.set_updater.deletions_planes.clear()
        bpy.context.scene.set_updater.updates_planes.clear()

        update_status = "UNDEFINED"

        return {'FINISHED'}

########################################################################

class PanelUpdatesChecking(bpy.types.Panel):
    bl_idname = "OBJECT_PT_updates_checking"
    bl_label = "Sets"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_parent_id = "PIPELINE_PT_lfs_scene_builder_panel"


    def draw(self, context):
        global update_status
        global keys

        if context.mode != 'OBJECT':
            row = self.layout.row()
            row.alignment = 'CENTER'
            row.label(icon='ERROR', text='Please go into Object mode')
            return

        if not "Sets" in bpy.context.scene.collection.children:
            row = self.layout.row()
            row.alignment = 'CENTER'
            row.label(icon='ERROR', text='No "Sets" collection found')
            return
        else:
            planes = [obj for obj in bpy.context.view_layer.objects
                  if 'camera_plane_distance' in obj]

            if not len(planes):
                row = self.layout.row()
                row.alignment = 'CENTER'
                row.label(icon='ERROR', text='No camera planes found in scene')
                return

        if update_status == "UNDEFINED":
            row = self.layout.row()
            row.operator("pipeline.check_updates", icon='FILEBROWSER')

        elif update_status == "NOTHING":
            row = self.layout.row()
            row.label(text="No update available")
            row = self.layout.row()
            row.operator("pipeline.updater_cancel",
                         icon='CHECKBOX_HLT',
                         text="OK")

        elif update_status == "READY":
            row = self.layout.row()
            row.operator("pipeline.updater_cancel", icon='CANCEL')
            row.operator("pipeline.apply_planes_updates", icon='IMPORT')

            if len(keys) > 1:
                self.layout.label(icon='ERROR',
                          text="WARNING. Images are coming from multiple files:")
                for key in keys:
                    col = self.layout.column(align=True)
                    row = col.row()
                    row.label(text="(" + str(len(keys[key])) + " items)  " + key)
                    for item in keys[key]:
                        row = col.row()
                        row.label(icon='DOT', text=item['name'])
                row = col.row()

        else:
            row = self.layout.row()
            row.label(text="Undefined behavior")


class SubPanelAdditions(bpy.types.Panel):
    bl_idname = "OBJECT_PT_sub_panel_addition"
    bl_label = "Plane additions"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_parent_id = "OBJECT_PT_updates_checking"

    @classmethod
    def poll(cls, context):
        return update_status == 'READY'

    def draw(self, context):
        row = self.layout.row()
        if len(bpy.context.scene.set_updater.additions_planes):
            row.label(text="Name")
            row.label(text="Action")
            for item in bpy.context.scene.set_updater.additions_planes:
                row = self.layout.row()
                row.label(text=item.name)
                row.prop(item, 'action', expand=True)
        else:
            row.label(text="Nothing to add")


class SubPanelDeletions(bpy.types.Panel):
    bl_idname = "OBJECT_PT_sub_panel_deletion"
    bl_label = "Plane deletions"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_parent_id = "OBJECT_PT_updates_checking"

    @classmethod
    def poll(cls, context):
        return update_status == 'READY'

    def draw(self, context):
        row = self.layout.row()
        if bpy.context.scene.set_updater.deletions_planes:
            row.label(text="Name")
            row.label(text="Action")
            for item in bpy.context.scene.set_updater.deletions_planes:
                row = self.layout.row()
                row.label(text=item.name)
                row.prop(item, 'action', expand=True)
        else:
            row.label(text="Nothing to delete")


class SubPanelUpdates(bpy.types.Panel):
    bl_idname = "OBJECT_PT_sub_panel_update"
    bl_label = "Plane updates"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_parent_id = "OBJECT_PT_updates_checking"

    @classmethod
    def poll(cls, context):
        return update_status == 'READY'

    def draw(self, context):
        if len(bpy.context.scene.set_updater.updates_planes):
            split = self.layout.split(factor=0.6)
            col = split.column()
            row = col.row()
            row.alignment = 'LEFT'
            row.label(text="Name")
            for item in bpy.context.scene.set_updater.updates_planes:
                row = col.row()
                row.alignment = 'LEFT'
                row.label(text=item.name)

            col = split.column()
            col.scale_x = 0.5
            row = col.row()
            row.alignment = 'RIGHT'
            row.label(text="File Version")
            for item in bpy.context.scene.set_updater.updates_planes:
                row = col.row()
                row.alignment = 'RIGHT'
                row.label(text=str(item.old_version))

            col = split.column()
            row = col.row()
            row.alignment = 'RIGHT'
            row.label(text="Disk Version")
            for item in bpy.context.scene.set_updater.updates_planes:
                row = col.row()
                row.alignment = 'RIGHT'
                row.label(text=str(item.new_version))

            col = split.column()
            row = col.row()
            row.alignment = 'RIGHT'
            row.label(text="Action")
            for item in bpy.context.scene.set_updater.updates_planes:
                row = col.row()
                row.alignment = 'RIGHT'
                row.prop(item, 'action', expand=True)
        else:
            self.layout.label(text="Nothing to update")


########################################################################

@persistent
def load_handler(dummy):

    global update_status
    global keys
    global to_be_updated_data
    global to_be_updated_visual
    global to_be_added
    global to_be_removed_visual
    global to_be_removed_data

    to_be_updated_data.clear()
    to_be_updated_visual.clear()
    to_be_added.clear()
    to_be_removed_visual.clear()
    to_be_removed_data.clear()
    keys.clear()

    bpy.context.scene.set_updater.additions_planes.clear()
    bpy.context.scene.set_updater.deletions_planes.clear()
    bpy.context.scene.set_updater.updates_planes.clear()

    update_status = "UNDEFINED"


classes = (DeletionPlanesData, AdditionPlanesData, UpdatePlanesData,
           PipelineData, UpdatesChecker, CancelUpdater, ApplyUpdatesOperator,
           PanelUpdatesChecking, SubPanelUpdates, SubPanelAdditions, SubPanelDeletions)

def register():
    for c in classes:
        bpy.utils.register_class(c)

    bpy.types.Scene.set_updater = bpy.props.PointerProperty(type=PipelineData)

    bpy.app.handlers.load_post.append(load_handler)


def unregister():
    del(bpy.types.Scene.set_updater)

    for c in classes:
        bpy.utils.unregister_class(c)

    bpy.app.handlers.load_post.remove(load_handler)

########################################################################
