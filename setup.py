# Copyright (C) 2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import bpy
from bpy.props import (BoolProperty, IntProperty, StringProperty,
                       CollectionProperty)
from mathutils import Vector, Euler, Quaternion
import os
import re
from .utils import (create_shadow_catcher_material,
                    assign_object_to_collection, traverse_tree,
                    get_versions_from_filepath, reset_bone_controllers,
                    convert_assets_to_overrides)
from .assets import ROOT_PATH
from .constraints_manager import reference_diff_constraints


class LFSSceneBuilderSetupScene(bpy.types.Operator):
    """Setup various basic settings in a scene"""
    bl_idname = "pipeline.scene_builder_setup"
    bl_label = "Setup Scene"
    bl_options = {'REGISTER', 'UNDO'}

    frame_start: IntProperty(min=0, default=101)
    frame_end: IntProperty(min=0, default=250)
    resolution_x: IntProperty(min=0, default=1920)
    resolution_y: IntProperty(min=0, default=1080)
    fps: IntProperty(min=0, default=24)

    create_camera: BoolProperty(default=True)
    create_shadow_catcher: BoolProperty(default=True)

    def execute(self, context):
        # Scene settings
        context.scene.frame_start = self.frame_start
        context.scene.frame_end = self.frame_end
        context.scene.render.resolution_x = self.resolution_x
        context.scene.render.resolution_y = self.resolution_y
        context.scene.render.fps = self.fps

        # Create camera rig and place it in collection
        if self.create_camera:
            name = 'Camera_render'
            bpy.ops.object.build_camera_rig(mode='2D')

            cam_rig = bpy.context.active_object
            cam_rig.name = name + "_rig"
            cam_rig.data.name = name + "_rig"

            cam = cam_rig.children[0]
            cam.name = name
            cam.data.name = name
            cam.hide_select = True

            # Collections
            assign_object_to_collection(cam, name)
            assign_object_to_collection(cam_rig, name)

            cam_rig.location = (0.0, 0.0, 0.0)
            cam_rig.pose.bones['Root'].location = (0.0, 1.5, 10.0)

        # Create shadow catcher plane and place it in collection
        if self.create_shadow_catcher:
            name = 'Shadow_catcher'
            plane = bpy.data.objects.new(name, bpy.data.meshes.new(name))
            plane.data.from_pydata([(-5.0, -5.0, 0.0),
                                    (-5.0, 5.0, 0.0),
                                    (5.0, 5.0, 0.0),
                                    (5.0, -5.0, 0.0)], [], [(0, 1, 2, 3)])

            material = create_shadow_catcher_material(name)
            plane.data.materials.append(material)

            sun_name = 'Sun'
            sun = bpy.data.objects.new(sun_name, bpy.data.lights.new(sun_name, 'SUN'))
            sun.location.z = 5
            sun.data.energy = 5.0

            coll = assign_object_to_collection(plane, name)
            assign_object_to_collection(sun, name)

            coll.hide_render = coll.hide_viewport = True

        return {'FINISHED'}


def cleanup_materials_in_collection(coll):
    mats = set()
    for obj in coll.all_objects[:]:
        # Get all materials in collection's objects and rename '.' to '_'
        for mat_slot in obj.material_slots:
            if mat_slot.material is not None:
                mats.add(mat_slot.material)

    for material in mats:
        mat_name = material.name
        if material.library is not None:
            material = material.copy().make_local()
        material.name = mat_name.replace('.', '_')


def export_assets_as_alembic(filepath):
    # Select only assets
    # TODO do not select controllers, deform, etc.
    for obj in bpy.data.objects:
        obj.select_set(False)

    convert_assets_to_overrides()

    high_colls = set()
    objects = set()

    # Make sure proper objects are selectable, visible and selected
    for type in ('Chars', 'Props'):
        if type in bpy.data.collections:
            coll = bpy.data.collections[type]
            cleanup_materials_in_collection(coll)
            for obj in coll.all_objects:
                if obj.type == 'MESH' and "cs_" not in obj.name:
                    for user_coll in obj.users_collection:
                        if "_high" in user_coll.name or "_geom" in user_coll.name:
                            layer_collections = {c.name: c for c in traverse_tree(bpy.context.view_layer.layer_collection)}
                            layer_collections[user_coll.name].hide_viewport = False
                            # Collect collections and objects, we'll select them later
                            high_colls.add(user_coll)
                            objects.add(obj)
                            break
                elif obj.type == 'EMPTY' and obj.instance_collection is not None:
                    cleanup_materials_in_collection(obj.instance_collection)
                    # Object is an instanced collection, probably a linked collection
                    obj.hide_select = False
                    obj.select_set(True)

    # Make collections selectable
    for coll in high_colls:
        if coll.hide_viewport:
            coll.hide_viewport = False
        if coll.hide_select:
            coll.hide_select = False

    # Select objects
    for obj in objects:
        obj.select_set(True)

    # Run Alembic export
    bpy.ops.wm.alembic_export(filepath=bpy.path.abspath(filepath), selected=True,
                              renderable_only=True, flatten=True, uvs=False,
                              packuv=False, normals=False, vcolors=False,
                              face_sets=False, use_instancing=True,
                              as_background_job=False)


def get_transparent_material():
    if "Ref_transparent" in bpy.data.materials:
        return bpy.data.materials.get("Ref_transparent")

    # Create material if not found
    mat = bpy.data.materials.new("Ref_transparent")
    mat.use_nodes = True
    mat.blend_method = 'BLEND'

    # Remove nodes from active node tree
    node_tree = mat.node_tree
    for node in node_tree.nodes:
        node_tree.nodes.remove(node)

    # Create nodes
    node = node_tree.nodes.new("ShaderNodeBsdfTransparent")
    node.location = (-180.0, 140.0)
    node.name = 'Transparent BSDF'
    node.inputs['Color'].default_value = (1.0, 1.0, 1.0, 1.0)

    node = node_tree.nodes.new("ShaderNodeBsdfDiffuse")
    node.location = (-180.0, 300.0)
    node.name = 'Diffuse BSDF'

    node = node_tree.nodes.new("ShaderNodeMixShader")
    node.location = (60.0, 300.0)
    node.name = 'Mix Shader'
    node.inputs['Fac'].default_value = 0.75

    node = node_tree.nodes.new("ShaderNodeOutputMaterial")
    node.location = (300.0, 300.0)
    node.name = 'Material Output'

    node_tree.links.new(node_tree.nodes['Mix Shader'].inputs[2],
                        node_tree.nodes['Transparent BSDF'].outputs[0])

    node_tree.links.new(node_tree.nodes['Mix Shader'].inputs[1],
                        node_tree.nodes['Diffuse BSDF'].outputs[0])

    node_tree.links.new(node_tree.nodes['Material Output'].inputs[0],
                        node_tree.nodes['Mix Shader'].outputs[0])

    return mat


def assign_transparent_material(obj):
    mat = get_transparent_material()
    obj.data.materials.clear()
    obj.data.materials.append(mat)


def import_alembic(context, filepath):
    # Import Alembic
    bpy.ops.wm.alembic_import(filepath=filepath,
                              relative_path=True,
                              set_frame_range=False,
                              validate_meshes=False,
                              is_sequence=False,
                              as_background_job=False)

    # Remove non-mesh objects
    for obj in context.selected_objects:
        if obj.type == 'MESH':
            # Assign objects to collection
            assign_object_to_collection(obj, "Ref")
            assign_transparent_material(obj)
        else:
            bpy.data.objects.remove(obj)

    # Hide ref collection
    if "Ref" in bpy.data.collections:
        bpy.data.collections['Ref'].hide_select = True
        bpy.data.collections['Ref'].hide_render = True


class LFSSceneBuilderAssetToImport(bpy.types.PropertyGroup):
    """Data necessary to import an asset.

    To be used for the anim setup operator.
    """
    filepath: StringProperty()
    name: StringProperty()
    target_collection: StringProperty()


class LFSSceneBuilderSetupAnimation(bpy.types.Operator):
    """Setup animation scene from layout scene"""
    bl_idname = "pipeline.scene_builder_setup_animation"
    bl_label = "Setup Animation Scene"
    bl_options = {'REGISTER', 'UNDO'}

    alembic_filepath: StringProperty(default="/tmp/test_export.abc",
                                     subtype='FILE_PATH')

    assets: CollectionProperty(type=LFSSceneBuilderAssetToImport)
    do_automate: BoolProperty(default=True)

    def invoke(self, context, event):
        if ("blocking" not in bpy.data.filepath
                and "breakdown" not in bpy.data.filepath):
            return context.window_manager.invoke_props_dialog(self)
        else:
            return self.execute(context)

    def execute(self, context):
        # Setup stuff in scene
        context.scene.eevee.use_taa_reprojection = False
        context.scene.render.use_simplify = True
        context.scene.render.simplify_subdivision = 0
        context.scene.tool_settings.use_keyframe_insert_auto = False

        # Fix render camera
        if context.scene.camera is not None:
            render_coll = bpy.data.collections.get(
                'Camera_render', bpy.data.collections.new('Camera_render'))
            if render_coll.name not in context.scene.collection.children:
                context.scene.collection.children.link(render_coll)

            cam = context.scene.camera
            prev_coll = cam.users_collection[0]

            if prev_coll.name != 'Camera_render':
                obj = cam
                obj.name = 'Camera_render'
                render_coll.objects.link(obj)
                prev_coll.objects.unlink(obj)
                while obj.parent is not None:
                    obj = obj.parent
                    render_coll.objects.link(obj)
                    prev_coll.objects.unlink(obj)

        alembic_filepath = self.alembic_filepath

        # Export and import back animation ghost
        if self.do_automate:
            if 'Chars' in bpy.data.collections or 'Props' in bpy.data.collections:
                export_assets_as_alembic(alembic_filepath)
                import_alembic(context, alembic_filepath)

        chars_exists = 'Chars' in bpy.data.collections
        props_exists = 'Props' in bpy.data.collections
        existing_assets = [a.name for a in context.scene.lfs_scene_builder_props.imported_assets]

        # Reimport assets
        for asset in self.assets:
            # Add suffix to coll name to avoid adding assets to existing (layout) collection
            if (asset.target_collection.capitalize() == 'Chars' and chars_exists
                    or asset.target_collection.capitalize() == 'Props' and props_exists):
                asset.target_collection += '-new'

            try:
                bpy.ops.pipeline.scene_builder_import_asset(
                    filepath=asset.filepath,
                    asset_name=asset.name,
                    target_collection=asset.target_collection)
            except RuntimeError:
                print(f"ASSET ERROR: could not import asset {asset.name} ({asset.filepath})")
                continue

            # TODO asset was imported using the proxy method and is tracked in scene

            # Get objects to copy animation
            dst_asset = context.scene.lfs_scene_builder_props.imported_assets[-1]
            dst_obj = dst_asset.proxy_object
            name = asset.name

            if self.do_automate:
                dst_asset.resolution = 'LOW'

            src_obj = None

            for existing_asset_name in existing_assets:
                if existing_asset_name.startswith(name):
                    existing_asset = context.scene.lfs_scene_builder_props.imported_assets[existing_asset_name]
                    if existing_asset.proxy_object is not None:
                        src_obj = existing_asset.proxy_object
                    else:
                        for o in existing_asset.object_datablocks:
                            if '_proxy' in o.name:
                                src_obj = o.datablock
                                break
                    existing_assets.remove(existing_asset_name)
                    break

            # Reapply position, anim and constraints for c_pos and c_traj
            if src_obj is not None and compare_armatures(src_obj, dst_obj):
                copy_animation(self, context, src_obj, dst_obj, all_bones=True)
                copy_constraints(self, context, src_obj, dst_obj, all_bones=True)

        # Relink broken library paths
        relink_library_paths(self, context)

        return {'FINISHED'}

    def draw(self, context):
        layout = self.layout
        if "anim" not in bpy.data.filepath:
            layout.label(icon='ERROR', text='You don’t seem to be in an animation scene. Proceed?')


###########################################
#
# Stuff to manually convert scene to anim
#
###########################################


class LFSSceneBuilderSetupAnimationConvertAlembic(bpy.types.Operator):
    """Export and reimport Alembic file from Props and Chars collection"""
    bl_idname = "pipeline.scene_builder_convert_alembic"
    bl_label = "Create Alembic Ghost"
    bl_options = {'REGISTER', 'UNDO'}

    filepath: StringProperty(maxlen=1024, subtype='FILE_PATH',
                             options={'HIDDEN', 'SKIP_SAVE'})

    def invoke(self, context, _event):
        sq = int(re.findall('sq([0-9]+)', bpy.data.filepath)[0])
        sc = int(re.findall('sc([0-9]+)', bpy.data.filepath)[0])
        filepath = os.path.abspath(bpy.path.abspath(f"//../../ref_layout_abc/v001/siren_sq{sq}_sc{sc:04}_ani_ref_layout.abc"))
        versions = get_versions_from_filepath(filepath, extension='.abc')
        if not versions:
            self.filepath = f"//../../ref_layout_abc/v001/siren_sq{sq}_sc{sc:04}_ani_ref_layout.abc"
        else:
            self.filepath = bpy.path.relpath(versions[0])

        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        # Export and import back animation ghost
        if 'Chars' in bpy.data.collections or 'Props' in bpy.data.collections:
            export_assets_as_alembic(self.filepath)
            import_alembic(context, self.filepath)
            return {'FINISHED'}
        else:
            self.report({'ERROR'}, "No Chars or Props collections found in scene")
            return {'CANCELLED'}


class LFSSceneBuilderSetupAnimationMoveToCollection(bpy.types.Operator):
    """Move selected objects to collection"""
    bl_idname = "pipeline.scene_builder_move_to_collection"
    bl_label = "Move to Collection"
    bl_options = {'REGISTER', 'UNDO'}

    collection: StringProperty(default="Chars")

    def execute(self, context):
        coll = context.collection

        dest = bpy.data.collections.get(
            self.collection, bpy.data.collections.new(self.collection))
        if dest.name not in bpy.context.scene.collection.children:
            bpy.context.scene.collection.children.link(dest)

        if coll.name in context.scene.collection.children:
            context.scene.collection.children.unlink(coll)
        for parent_coll in bpy.data.collections:
            if coll.name in parent_coll.children:
                parent_coll.children.unlink(coll)

        dest.children.link(coll)

        # objects = []
        # for obj in context.selected_objects:
        #     objects.extend(traverse_tree(obj))
        # for obj in objects:
        #     assign_object_to_collection(obj, self.collection)
        return {'FINISHED'}


class LFSSceneBuilderSetupAnimationRenameCollectionGeom(bpy.types.Operator):
    """Move selected objects to collection"""
    bl_idname = "pipeline.scene_builder_rename_collection_geom"
    bl_label = "Rename to _geom"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        coll = context.collection
        coll.name += "_geom"
        return {'FINISHED'}


def relink_library_paths(op, context):
    """Relink broken link in library"""
    root_path = os.path.join(ROOT_PATH[0], "").replace("\\", "\\\\")

    if not root_path or not os.path.isdir(root_path):
        op.report({'ERROR'}, 'Root not found, please set Root Path in Scene Builder add-on preferences')
        return {'CANCELLED'}

    for img in bpy.data.images:
        if re.search(r".*[/\\]lib[/\\]", img.filepath):
            new_filepath = re.sub(r".*?[/\\]lib[/\\]", root_path, img.filepath)
            try:
                if (os.path.abspath(bpy.path.abspath(img.filepath)) !=
                        os.path.abspath(bpy.path.abspath(new_filepath))):
                    img.filepath = new_filepath
                    op.report({'INFO'}, f"Remapped image {img.name} to {new_filepath}")
            except:
                print("Broken image: ", img.name, img.filepath)

    for cache in bpy.data.cache_files:
        if re.search(r".*[/\\]lib[/\\]", cache.filepath):
            new_filepath = re.sub(r".*?[/\\]lib[/\\]", root_path, cache.filepath)
            if (os.path.abspath(bpy.path.abspath(cache.filepath)) !=
                    os.path.abspath(bpy.path.abspath(new_filepath))):
                cache.filepath = new_filepath
                cache.reload()
                op.report({'INFO'}, f"Remapped cache {cache.name} to {new_filepath}")

    for lib in bpy.data.libraries:
        if re.search(r".*[/\\]lib[/\\]", lib.filepath):
            new_filepath = re.sub(r".*?[/\\]lib[/\\]", root_path, lib.filepath)
            if (os.path.abspath(bpy.path.abspath(lib.filepath)) !=
                    os.path.abspath(bpy.path.abspath(new_filepath))):
                lib.filepath = new_filepath
                lib_name = lib.name
                lib.reload()
                op.report({'INFO'}, f"Remapped lib {lib_name} to {new_filepath}")

    bpy.ops.file.make_paths_relative()


def remove_unused_assets():
    """Remove assets not actually used in the scene"""
    for asset in reversed(bpy.context.scene.lfs_scene_builder_props.imported_assets):
        do_delete = True
        if asset.import_type == 'PROXY':
            if (asset.proxy_object is not None and len(asset.proxy_object.users_scene)
                    or asset.instance_object is not None and len(asset.instance_object.users_scene)):
                do_delete = False
        elif asset.import_type == 'OVERRIDE':
            for obj in asset.object_datablocks:
                if obj.datablock is not None and len(obj.datablock.users_scene):
                    do_delete = False
                    break

        if do_delete:
            print(asset, asset.name)
            bpy.ops.pipeline.scene_builder_remove_asset(asset_name=asset.name)


def remove_unused_libraries():
    """Remove libraries not actually used in the scene"""
    for lib in bpy.data.libraries:
        do_remove = True
        for db in lib.users_id:
            if db.users > 1 or not db.use_fake_user:
                do_remove = False
                break
        if do_remove:
            bpy.data.libraries.remove(lib)


def sort_collections(context):
    """Sort collections in the order: Chars, Props, Grease_Pencil, Sets, Camera_render, Ref, Shadow_catcher, Widgets"""
    parent_coll = context.scene.collection
    parent_layer_coll = context.view_layer.layer_collection

    children = []
    order = ('RENDER', 'Chars', 'Props', 'Grease_Pencil', 'Sets', 'Camera_render', 'Ref',
             'Shadow_catcher', 'Widgets')

    for child_name in order:
        if child_name in parent_coll.children:
            children.append(parent_coll.children[child_name])
        if child_name + '-new' in parent_coll.children:
            children.append(parent_coll.children[child_name + '-new'])

    for child in parent_coll.children:
        if child not in children:
            children.append(child)

    for child in children:
        is_exclude = parent_layer_coll.children[child.name].exclude
        parent_coll.children.unlink(child)
        parent_coll.children.link(child)
        parent_layer_coll.children[child.name].exclude = is_exclude


class LFSSceneBuilderSetupAnimationCleanup(bpy.types.Operator):
    """Cleanup scene post conformation"""
    bl_idname = "pipeline.scene_builder_cleanup"
    bl_label = "Cleanup"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        sort_collections(context)
        relink_library_paths(self, context)
        for i in range(10):
            bpy.ops.outliner.orphans_purge()
        remove_unused_assets()
        remove_unused_libraries()
        bpy.ops.file.make_paths_relative()

        # Cleanup collection restriction toggles
        for coll in context.view_layer.layer_collection.children:
            if coll.name == 'Shadow_catcher':
                if coll.exclude:
                    coll.collection.hide_viewport = True
                    coll.collection.hide_render = True
            elif coll.name == 'Widgets':
                coll.exclude = True
                coll.collection.hide_viewport = True
                coll.collection.hide_render = True

            if coll.name in {'Sets', 'Camera_render', 'Shadow_catcher', 'Widgets'}:
                coll.collection.hide_select = True
        return {'FINISHED'}


def copy_animation(self, context, src_obj, dst_obj, all_bones=True):
    if all_bones:
        bone_names = [b.name for b in src_obj.pose.bones if b.name.startswith("c_")]
    else:
        bone_names = ("c_pos", "c_traj", "c_mov", "c_rot")

    # Copy transforms and props
    for name in bone_names:
        if name in dst_obj.pose.bones and name in src_obj.pose.bones:
            dst_pb = dst_obj.pose.bones[name]
            src_pb = src_obj.pose.bones[name]
            dst_pb.matrix_basis = src_pb.matrix_basis
            for prop in src_pb.keys():
                dst_pb[prop] = src_pb[prop]

    if src_obj.animation_data is not None and src_obj.animation_data.action is not None:
        dst_action = src_obj.animation_data.action.copy()
        dst_action.name = dst_obj.name + "Action"

        # Remove bones not part of the main controllers
        if not all_bones:
            for fcurve in dst_action.fcurves[:]:
                if not any(name in fcurve.data_path for name in bone_names):
                    dst_action.fcurves.remove(fcurve)
    else:
        dst_action = bpy.data.actions.new(dst_obj.name + "Action")

    dst_obj.animation_data_create().action = dst_action


def compare_armatures(src_arm, dst_arm):
    """Check that no bones have been removed, based on bones' names"""
    src_bones = {b.name for b in src_arm.pose.bones if not b.bone.use_deform and not b.name.startswith("mch_")}
    dst_bones = {b.name for b in dst_arm.pose.bones if not b.bone.use_deform and not b.name.startswith("mch_")}
    return len(src_bones - dst_bones) == 0


def copy_constraints(self, context, src_obj, dst_obj, all_bones=True):
    if all_bones:
        bone_names = [b.name for b in src_obj.pose.bones if b.name.startswith("c_")]
    else:
        bone_names = ("c_pos", "c_traj", "c_mov", "c_rot")

    dst_obj.lfs_scene_builder_ref_arma = src_obj
    src_obj.lfs_scene_builder_new_arma = dst_obj

    # Copy constraints
    for name in bone_names:
        if name in dst_obj.pose.bones and name in src_obj.pose.bones:
            src_pbone = src_obj.pose.bones[name]
            dst_pbone = dst_obj.pose.bones[name]
            objects_found = 0
            for con in src_pbone.constraints:
                if con.name not in dst_pbone.constraints:
                    con_dst = dst_pbone.constraints.copy(con)

                    # Try to find target object
                    # TODO do it in a way that doesn't suck
                    if hasattr(con, 'target') and con.target is not None:
                        target_obj = None
                        if con.target is src_obj:
                            target_obj = dst_obj
                        else:
                            org_target_obj_name = con.target.name
                            for obj in context.view_layer.objects:
                                if (obj is not con.target
                                        and obj.type == 'ARMATURE'
                                        and "_proxy" in obj.name
                                        and obj.name.startswith(org_target_obj_name.split("_")[0])):
                                    if target_obj is None:
                                        target_obj = obj
                                    objects_found += 1
                            if objects_found > 1:
                                self.report({'WARNING'}, f'Found more than one candidate target, check constraint for {dst_obj.name}, bone {src_pbone.name}')

                        if target_obj is not None:
                            con_dst.target = target_obj

    reference_diff_constraints(src_obj, dst_obj)


#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
#             STRING TO BE ABLE TO PASS A STRING BY REFERENCE                       #
#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #


class String():
    def __init__(self, data=""):
        self.string = data

    def __str__(self):
        return self.string

    def __iadd__(self, other):
        self.string += other.string

    def __len__(self):
        return len(self.string)

    def set(self, value):
        self.string = value

    def split(self, char):
        splitted = self.string.split(char)
        return [String(i) for i in splitted]


#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
#             EXTRACTS THE PATH OF AN OBJECT THROUGH THE SCENE TREE                 #
#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #


def locate_path(object, path, result):
    collection = None

    if len(path) == 0:
        collection = bpy.context.scene.collection
    else:
        splitted_path = path.split('<§>')
        last_found = splitted_path[-1]
        collection = bpy.data.collections.get(last_found)

    if collection is not None:
        if object.name in collection.objects:
            result.set(path)
        else:
            for c in collection.children:
                locate_path(object, path + "<§>" + c.name, result)


#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
#                  DETECT COLLECTION BY NAME OR CLOSE NAME                          #
#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #


def detect_collection(desired, possible):
    collection = bpy.context.scene.collection.children.get(desired)
    if collection is not None:
        return True, collection
    else:
        found = []
        formatted = [c.name.lower() for c in bpy.context.scene.collection.children]

        for i, f in enumerate(formatted):
            for p in possible:
                if p in f and not (f.startswith("_") or f.endswith("_")):
                    found.append(i)

        if len(found) == 0:
            return False, None
        elif len(found) == 1:
            return True, bpy.context.scene.collection.children[found[0]]
        else:
            return True, None


#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
#             SORT ITEMS OG THE SCENE IN THE CORRECT CATEGORY                       #
#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #


def detect_content(operator):
    for asset_type in ('Chars', 'Props'):
        asset_collections = []

        # Get asset collections which need to be moved
        for collection in bpy.context.scene.collection.children:
            if collection.override_library is not None:
                path = collection.override_library.reference.library.filepath
                path = path.lower().replace('\\', '/')
                if f"lib/{asset_type.lower()}" in path:
                    asset_collections.append(collection)

        # Create collection and move children there
        if asset_collections:
            status, coll_asset_type = detect_collection(asset_type, [asset_type[:-1].lower()])
            if not status:
                coll_asset_type = bpy.data.collections.new(asset_type)
                bpy.context.scene.collection.children.link(coll_asset_type)
                operator.report({'INFO'}, f'{asset_type} collection was added')
            else:
                if coll_asset_type is None:
                    operator.report({'WARNING'}, f'Too many candidates for the {asset_type} collection')
                else:
                    name = coll_asset_type.name
                    operator.report({'INFO'}, f"Collection '{name}' has been renamed '{asset_type}'")
                    coll_asset_type.name = asset_type

            for collection in asset_collections:
                bpy.context.scene.collection.children.unlink(collection)
                coll_asset_type.children.link(collection)


#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
#               MOVE SELECTED OBJECTS TO A _HIGH COLLECTION                         #
#  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #


def move_selection_to_geom(operator):
    names = []
    collection_name = ""
    for object in bpy.context.selected_objects:
        names.append(object.name)
        path = String()
        locate_path(object, "", path)
        path_object = [bpy.data.collections.get(c) for c in str(path).split('<§>') if (len(c) > 0)]
        path_simple = [c.name for c in path_object]
        print("PATH: ", path_object)
        if ('Chars' in path_simple) or ('Props' in path_simple):
            if path_object[-1].override_library is None:
                # path_object[0] == Chars or Props
                collection = bpy.data.collections.get(path_object[1].name + "_high")
                if collection is None:
                    collection = bpy.data.collections.new(path_object[1].name + "_high")
                    path_object[1].children.link(collection)
                    collection_name = collection.name
                path_object[-1].objects.unlink(object)
                collection.objects.link(object)
            else:
                operator.report({'WARNING'}, 'The asset collection should be local')
                return
        else:
            operator.report({'WARNING'}, 'Assets must be sorted before using this operator')

    operator.report({'INFO'}, 'Objects {} were moved to {}'.format(", ".join(names), collection_name))


class LFSSceneBuilderSetupAnimationSortAssets(bpy.types.Operator):
    """ Sort assets in the correct category """

    bl_idname = "pipeline.sort_assets"
    bl_label = "Sort Assets By Category"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        detect_content(self)
        return {'FINISHED'}


class LFSSceneBuilderSetupAnimationMoveGeometry(bpy.types.Operator):
    """ Move meshes to the _high collection """

    bl_idname = "pipeline.move_geometry"
    bl_label = "Move Mesh To High"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        move_selection_to_geom(self)
        return {'FINISHED'}


class LFSSceneBuilderSetupAnimationCopyAnim(bpy.types.Operator):
    """Copy animation and constraints"""
    bl_idname = "pipeline.scene_builder_copy_anim"
    bl_label = "Copy Animation Keys"
    bl_options = {'REGISTER', 'UNDO'}

    all_bones: BoolProperty(default=True)

    def execute(self, context):
        dst_obj = context.active_object
        src_obj = next(o for o in context.selected_objects if o is not dst_obj)

        copy_animation(self, context, src_obj, dst_obj, all_bones=self.all_bones)
        return {'FINISHED'}


class LFSSceneBuilderSetupAnimationCopyConstraints(bpy.types.Operator):
    """Copy animation and constraints"""
    bl_idname = "pipeline.scene_builder_copy_constraints"
    bl_label = "Copy Constraints"
    bl_options = {'REGISTER', 'UNDO'}

    all_bones: BoolProperty(default=True)

    def execute(self, context):
        dst_obj = context.active_object
        src_obj = next(o for o in context.selected_objects if o is not dst_obj)

        copy_constraints(self, context, src_obj, dst_obj, all_bones=self.all_bones)
        return {'FINISHED'}


class LFSSceneBuilderSetupAnimationCleanupAnim(bpy.types.Operator):
    """Delete additional animation"""
    bl_idname = "pipeline.scene_builder_cleanup_anim"
    bl_label = "Keep Only Basic Controllers’ Animation"
    bl_options = {'REGISTER', 'UNDO'}

    asset_name: StringProperty(name="Asset Name", description="Name of the asset to cleanup")
    do_all_assets: BoolProperty(name="Do all assets",)

    def execute(self, context):
        if self.do_all_assets:
            assets = context.scene.lfs_scene_builder_props.imported_assets
        else:
            assets = {context.scene.lfs_scene_builder_props.imported_assets[self.asset_name]}

        is_changed = False
        for asset in assets:
            proxy_object = asset.proxy_object
            bone_names = ("c_pos", "c_traj", "c_mov", "c_rot")

            if (proxy_object is not None
                    and proxy_object.animation_data.action is not None
                    and proxy_object.animation_data is not None):
                action = proxy_object.animation_data.action
                for fcurve in action.fcurves[:]:
                    if not any(name in fcurve.data_path for name in bone_names):
                        action.fcurves.remove(fcurve)
                        is_changed = True
                # Reset bone transforms
                # TODO better reset all props
                for pbone in proxy_object.pose.bones:
                    pbone.location = Vector()
                    pbone.rotation_euler = Euler()
                    pbone.rotation_quaternion = Quaternion()
                    pbone.scale = Vector((1.0, 1.0, 1.0))
                    is_changed = True
                    reset_bone_controllers(pbone)

        if is_changed:
            context.scene.frame_set(context.scene.frame_current)
        return {'FINISHED'}


class PIPELINE_PT_lfs_scene_builder_anim_build_panel(bpy.types.Panel):
    bl_idname = "PIPELINE_PT_lfs_scene_builder_anim_build_panel"
    bl_label = "Animation Scene Builder"
    bl_category = "LFS"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'

    @classmethod
    def poll(cls, context):
        addon_prefs = context.preferences.addons[__package__].preferences
        return addon_prefs.show_confo

    def draw(self, context):
        layout = self.layout

        # row = layout.row(align=True)
        # for dest in ('Chars', 'Props'):
        #     op = row.operator("pipeline.scene_builder_move_to_collection", text=f"Move to Collection {dest}")
        #     op.collection = dest

        # col = layout.column(align=True)
        # col.operator("pipeline.sort_assets", icon='SORTALPHA')

        # row = layout.row(align=True)
        # # row.operator("pipeline.move_geometry", icon='MESH_DATA')
        # row.operator("pipeline.scene_builder_rename_collection_geom", icon='MESH_DATA')

        col = layout.column(align=True)
        col.operator("pipeline.scene_builder_convert_alembic", icon='GHOST_ENABLED')

        col = layout.column(align=True)
        row = col.row()
        row.operator("pipeline.scene_builder_copy_anim", icon='GRAPH').all_bones=True
        row.operator("pipeline.scene_builder_copy_constraints", icon='CONSTRAINT_BONE').all_bones=True
        row = col.row()
        row.operator("pipeline.scene_builder_copy_anim", icon='MOD_SUBSURF', text='Copy Main Controllers').all_bones=False

        col = layout.column(align=True)
        col.operator("pipeline.scene_builder_cleanup", icon='SHADERFX')


classes = (LFSSceneBuilderAssetToImport,
           LFSSceneBuilderSetupAnimationMoveToCollection,
           LFSSceneBuilderSetupAnimationRenameCollectionGeom,
           LFSSceneBuilderSetupAnimationConvertAlembic,
           LFSSceneBuilderSetupAnimationCopyAnim,
           LFSSceneBuilderSetupAnimationCopyConstraints,
           LFSSceneBuilderSetupAnimationCleanupAnim,
           LFSSceneBuilderSetupAnimationCleanup,
           LFSSceneBuilderSetupAnimationSortAssets,
           LFSSceneBuilderSetupAnimationMoveGeometry,
           PIPELINE_PT_lfs_scene_builder_anim_build_panel)


def register():
    for c in classes:
        bpy.utils.register_class(c)

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
