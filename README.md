# Scene builder
This Blender add-on helps in creating Blender scenes for film shots,
using information from an asset database.

## License

Blender scripts published by **Les Fées Spéciales** are, except where
otherwise noted, licensed under the GPLv2 license.
