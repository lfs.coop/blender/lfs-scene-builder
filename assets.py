# Copyright (C) 2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import bpy
from bpy.props import StringProperty, BoolProperty
from bpy.app.handlers import persistent
import os
import re

from .utils import (traverse_tree, get_versions_from_filepath,
                    relink_constraints, get_child_recursively)

ROOT_PATH = [""]


def link_collection(op, filepath, asset_name):
    """Link in the collection called asset_name"""
    with bpy.data.libraries.load(filepath, link=True,
                                 relative=True) as (data_from, data_to):
        for coll in data_from.collections:
            if coll == asset_name:
                data_to.collections.append(coll)
                break
        linked_collections = data_to.collections
    if len(linked_collections) == 0:
        op.report({'WARNING'}, f'Asset "{asset_name}" not found')
        return {'CANCELLED'}
    return data_to


def add_to_scene_assets(context, asset_name, actual_name):
    """Add the asset to the lfs_scene_builder_props"""
    asset = context.scene.lfs_scene_builder_props.imported_assets.add()
    asset.asset_name = asset_name

    # Avoid duplicate names
    unique_asset_name = actual_name  # Actual name, if multiple in scene
    i = 1
    import re
    while unique_asset_name in context.scene.lfs_scene_builder_props.imported_assets:
        matches = re.findall(r"(.*)\.[0-9]+$", unique_asset_name)  # Strip trailing number
        if matches:
            unique_asset_name = matches[0]
        unique_asset_name = f"{unique_asset_name}.{i:03}"
        i += 1

    asset.name = unique_asset_name
    return asset


def create_instance_object(op, context, data_to, asset_name):
    # Get the original collection (from library)
    original_collection = [c for c in data_to.collections if c.name == asset_name][0]
    # if original_collection.override_library is not None:
    #     # This allows avoiding name conflicts if the collection has previously been overridden
    #     original_collection = bpy.data.collections[asset_name].override_library.reference

    if asset_name + "_rig" not in original_collection.all_objects:
        op.report({'WARNING'}, f'Rig "{asset_name + "_rig"}" not found')
        return {'CANCELLED'}, None

    coll_instance = bpy.data.objects.new(asset_name, None)
    coll_instance.instance_type = 'COLLECTION'
    coll_instance.instance_collection = original_collection

    # Link base imported collection. If no target collection is specified,
    # put it straight under the Scene Collection
    target_collection_name = op.target_collection.capitalize()
    if target_collection_name:
        target_collection = bpy.data.collections.get(target_collection_name,
                                               bpy.data.collections.new(target_collection_name))
        if target_collection.name not in bpy.context.scene.collection.children:
            bpy.context.scene.collection.children.link(target_collection)
        target_collection.objects.link(coll_instance)

        # Make target collection visible, so stuff may be selected
        target_collection.hide_viewport = False
        target_collection.hide_select = False
        target_layer_collection = get_child_recursively(target_collection_name, context.view_layer.layer_collection)
        target_layer_collection.exclude = False
        target_layer_collection.hide_viewport = False
    else:
        bpy.context.scene.collection.objects.link(coll_instance)

    # Select collection instance
    for obj in context.scene.objects:
        obj.select_set(False)
    coll_instance.select_set(True)
    bpy.context.view_layer.objects.active = coll_instance

    # TODO get rig name
    bpy.ops.object.proxy_make(object=asset_name + "_rig")
    proxy_object = context.active_object

    coll_instance.hide_select = True
    return coll_instance, proxy_object


def is_asset_selected(asset):
    """ Get whether the armature from a given asset is selected in scene"""
    if asset.import_type == 'PROXY' and asset.proxy_object is not None:
        return asset.proxy_object.select_get()
    else:
        for obj_db in asset.object_datablocks:
            if obj_db.datablock is not None and obj_db.datablock.select_get():
                return True
    return False


class LFSSceneBuilderImportAsset(bpy.types.Operator):
    """Import asset from server"""
    bl_idname = "pipeline.scene_builder_import_asset"
    bl_label = "Import Asset"
    bl_options = {'REGISTER', 'UNDO'}

    asset_name: StringProperty(name="Asset Name", description="Name of the collection to link")
    target_collection: StringProperty(name="Collection",
                                      description="Collection in which to move the asset")

    filepath: StringProperty(
        name="File Path",
        description="Filepath used for importing the file",
        maxlen=1024,
        subtype='FILE_PATH')

    def execute(self, context):
        # Convert existing copies to overrides
        filepath = os.path.abspath(bpy.path.abspath(self.filepath))
        # If no asset name specified, try with the blend file
        # TODO handle case?
        if self.asset_name == "":
            self.asset_name = os.path.basename(filepath).split("_")[0]

        # Check that asset hasn't yet been imported, and if so, override existing proxy copy
        assets = context.scene.lfs_scene_builder_props.imported_assets
        for existing_asset in assets:
            if existing_asset.import_type == 'PROXY':
                library = get_library(existing_asset)
                if library is not None and os.path.abspath(bpy.path.abspath(library.filepath)) == filepath:
                    bpy.ops.pipeline.scene_builder_convert_to_override(asset_name=existing_asset.name)

        filepath = bpy.path.relpath(self.filepath)

        data_to = link_collection(self, filepath, self.asset_name)
        if type(data_to) == set:
            return data_to

        instance_obj, proxy_object = create_instance_object(self, context,
                                                            data_to, self.asset_name)
        # Exit if instance could not be created
        if type(instance_obj) is set:
            return instance_obj

        asset = add_to_scene_assets(context, self.asset_name, instance_obj.name)

        original_collection = [c for c in data_to.collections if c.name == self.asset_name][0]

        asset.library = original_collection.library
        asset.instance_object = instance_obj
        asset.proxy_object = proxy_object

        return {'FINISHED'}


def get_library(asset):
    """
    Try to find the library for the asset, in case it was lost or not properly
    defined for some reason
    """
    if asset.library is not None:
        return asset.library
    elif asset.import_type == 'PROXY':
        if (asset.proxy_object is not None
                and asset.proxy_object.proxy is not None):
            return asset.proxy_object.proxy.library
        elif (asset.instance_object is not None
                  and asset.instance_object.instance_collection is not None):
            return asset.instance_object.instance_collection.library
    elif (asset.import_type == 'APPEND'
          and len(asset.object_datablocks)
          and asset.object_datablocks[0].datablock is not None):
        return asset.object_datablocks[0].datablock.override_library.reference.library


class ChangeAssetMixin(bpy.types.Operator):
    """Base op class, to be used by the Replace and Update operators """
    bl_options = {'REGISTER', 'UNDO'}

    asset_name: StringProperty(name="Asset Name", description="Name of the collection to link")
    do_all_assets: BoolProperty(name="Update All", default=False)

    def invoke(self, context, event):
        if hasattr(self, "do_all_assets") and self.do_all_assets:
            return context.window_manager.invoke_confirm(self, event)
        else:
            return context.window_manager.invoke_props_dialog(self)

    def execute(self, context):
        if self.do_all_assets:
            for asset in context.scene.lfs_scene_builder_props.imported_assets:
                if asset.is_updatable:
                    self.asset_name = asset.name
                    self.update_asset(context, asset)
        else:
            new_filepath = self.get_new_filepath()
            if not new_filepath:
                self.report({'WARNING'}, "New file invalid")
                return {'CANCELLED'}
            asset = context.scene.lfs_scene_builder_props.imported_assets[self.asset_name]
            self.update_asset(context, asset)
        return {'FINISHED'}

    def get_new_filepath(self):
        raise NotImplementedError

    def reload_lib(self, context, asset):
        raise NotImplementedError

    def update_asset(self, context, asset):
        # TODO Make that work for overriden assets

        # Make temporary copy, to reapply and refresh transforms, otherwise
        # we're stuck with the rest pose (?!)
        if asset.import_type == 'PROXY':
            proxy_object = asset.proxy_object
            tmp_copy_object = proxy_object.copy().make_local()
            tmp_copy_object.data = tmp_copy_object.data.copy().make_local()

            relink_constraints(proxy_object, tmp_copy_object)
            bpy.data.objects.remove(proxy_object)

        self.reload_lib(context, asset)

        if asset.import_type == 'PROXY':
            new_proxy_object = context.active_object
            asset.proxy_object = new_proxy_object

            # Reapply transforms and animation
            for pb in tmp_copy_object.pose.bones:
                if pb.name.startswith('c_') and pb.name in new_proxy_object.pose.bones:
                    new_pb = new_proxy_object.pose.bones[pb.name]
                    new_pb.matrix_basis = pb.matrix_basis
                    for prop in pb.keys():
                        new_pb[prop] = pb[prop]

            # Reapply constraints
            relink_constraints(tmp_copy_object, new_proxy_object)

            new_proxy_object.animation_data_create()
            if tmp_copy_object.animation_data is not None:
                new_proxy_object.animation_data.action = tmp_copy_object.animation_data.action
                for prop in {'action_blend_type', 'action_extrapolation', 'action_influence',}:
                    setattr(new_proxy_object.animation_data, prop,
                            getattr(tmp_copy_object.animation_data, prop))

            for src_track in tmp_copy_object.animation_data.nla_tracks:
                dst_track = new_proxy_object.animation_data.nla_tracks.new()
                dst_track.name = src_track.name
                for prop in {'lock', 'mute', 'select', 'is_solo'}:
                        setattr(dst_track, prop, getattr(src_track, prop))
                for src_strip in src_track.strips:
                    dst_strip = dst_track.strips.new(src_strip.name, src_strip.frame_start, src_strip.action)
                    for prop in {'blend_in', 'blend_out', 'blend_type',
                                 'action_frame_start', 'action_frame_end',
                                 'extrapolation', 'frame_start', 'frame_end',
                                 'influence', 'mute', 'repeat', 'scale',
                                 'select', 'use_animated_influence',
                                 'use_animated_time',
                                 'use_animated_time_cyclic', 'use_auto_blend',
                                 'use_reverse', 'use_sync_length'}:
                        setattr(dst_strip, prop, getattr(src_strip, prop))

            # Delete proxy object
            armature = tmp_copy_object.data
            bpy.data.objects.remove(tmp_copy_object)
            bpy.data.armatures.remove(armature)

            # Workaround to force refresh...
            bpy.context.view_layer.update()
            asset.instance_object.hide_select = asset.instance_object.hide_select
        update_updatable(context)

def get_available_versions(asset):
    filepath = None
    library = get_library(asset)
    if library is not None:
        filepath = library.filepath
    if filepath is None:
        return ()

    return get_versions_from_filepath(filepath, extension=".blend")

class LFSSceneBuilderUpdateAsset(ChangeAssetMixin):
    """Update asset from server"""
    bl_idname = "pipeline.scene_builder_update_asset"
    bl_label = "Update Asset"

    def asset_version_items(self, context):
        asset = context.scene.lfs_scene_builder_props.imported_assets[self.asset_name]
        versions = get_available_versions(asset)
        versions = [(v, os.path.basename(os.path.dirname(v)), v) for v in versions]
        return versions

    asset_name: StringProperty(name="Asset Name", description="Name of the collection to link")
    do_all_assets: BoolProperty(name="Update All", default=False)
    version: bpy.props.EnumProperty(name="Version", items=asset_version_items)

    def get_new_filepath(self):
        if self.do_all_assets:
            self.version = self.asset_version_items(bpy.context)[0][0]
        return bpy.path.relpath(self.version)

    def reload_lib(self, context, asset):
        """Change the asset lib filepath to the selected version"""
        library = get_library(asset)
        new_filepath = self.get_new_filepath()
        library.filepath = new_filepath
        library.reload()
        if asset.library is None:
            asset.library = library

        # Recreate proxy and reapply anim
        if asset.import_type == 'PROXY':
            # Select collection instance
            for obj in context.scene.objects:
                obj.select_set(False)
            asset.instance_object.select_set(True)
            context.view_layer.objects.active = asset.instance_object

            bpy.ops.object.proxy_make(object=asset.asset_name + "_rig")

            asset.proxy_object = context.active_object
            asset.library = asset.proxy_object.proxy.library

    def draw(self, context):
        layout = self.layout
        versions = self.asset_version_items(context)
        if len(versions):
            layout.prop(self, "version")
        else:
            layout.label(text="No version found", icon='ERROR')


class LFSSceneBuilderReplaceAsset(ChangeAssetMixin):
    """Replace asset from server"""
    bl_idname = "pipeline.scene_builder_replace_asset"
    bl_label = "Replace Asset"

    asset_name: StringProperty(name="Asset Name", description="Name of the collection to link")
    asset_path: StringProperty(name="Asset Path", description="Path to the new asset file",
                               default='')

    def get_new_filepath(self):
        asset_path = self.asset_path
        if asset_path:
            # Add filler at the end to match get_versions_from_filepath()'s logic
            asset_path = os.path.join(ROOT_PATH[0], asset_path, "rigging",
                                      "rig_ok_blend", "v001", "_.blend")

            versions = get_versions_from_filepath(asset_path, extension=".blend")
            if len(versions):
                return versions[0]
            else:
                return ''
        else:
            return ''

    def reload_lib(self, context, asset):
        """Link in new asset"""
        asset_type, asset_family, asset_name = self.asset_path.split("/")

        # Load new lib if it was replaced
        if (asset_type != asset.asset_type
                or asset_family != asset.asset_family
                or asset_name != asset.asset_name):
            filepath = self.get_new_filepath()
            data_to = link_collection(self, filepath, asset_name)
            if type(data_to) == set:
                return data_to
            asset.asset_type = asset_type
            asset.asset_family = asset_family
            asset.asset_name = asset_name

        self.target_collection = asset.instance_object.users_collection[0].name
        bpy.data.objects.remove(asset.instance_object)

        instance_obj, proxy_object = create_instance_object(self, context, data_to, asset_name)
        asset.instance_object = instance_obj
        asset.proxy_object = proxy_object
        asset.library = proxy_object.proxy.library

    def draw(self, context):
        layout = self.layout
        filepath = draw_asset_selection(self, context, layout)
        if filepath is not None:
            layout.label(text=filepath)


class LFSSceneBuilderConvertToOverride(bpy.types.Operator):
    """Convert proxy to override"""
    bl_idname = "pipeline.scene_builder_convert_to_override"
    bl_label = "Convert to Override"
    bl_options = {'REGISTER', 'UNDO'}

    asset_name: StringProperty(name="Asset Name", description="Name of the asset to remove")
    make_local: bpy.props.BoolProperty(name="Make Local", default=False)

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)

    def execute(self, context):
        asset = context.scene.lfs_scene_builder_props.imported_assets[self.asset_name]
        instance_object = asset.instance_object
        proxy_object = asset.proxy_object

        if bpy.ops.object.mode_set.poll():
            bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

        if asset.import_type == 'PROXY':
            if instance_object is None or proxy_object is None or proxy_object.proxy is None:
                self.report({'WARNING'}, f'Asset "{self.asset_name}" not found')
                return {'CANCELLED'}

            # Store collections used by proxy, to remove it later
            previous_collections = proxy_object.users_collection
            previous_collection_children = set()
            for coll in previous_collections:
                previous_collection_children.update(coll.children)

            # Select proxy object
            for obj in context.scene.objects:
                obj.select_set(False)

            # Get collections and show them, otherwise we can't select the object and convert it to override
            for coll in proxy_object.users_collection:
                layer_coll = get_child_recursively(coll.name, context.view_layer.layer_collection)
                if layer_coll is not None:
                    layer_coll.exclude = False
            proxy_object.select_set(True)

            # Convert proxy to override and unlink from its collection
            bpy.context.view_layer.objects.active = proxy_object
            bpy.ops.object.convert_proxy_to_override()
            for coll in previous_collections:
                coll.objects.unlink(proxy_object)

            # Get new collections (TODO find less stupid way?)
            new_collection_children = set()
            for coll in previous_collections:
                new_collection_children.update(coll.children)

            # Assign objects and collections to scene structure, for later tracking
            new_collection = list(new_collection_children - previous_collection_children)[0]
            for coll in traverse_tree(new_collection):
                collection_datablock = asset.collection_datablocks.add()
                collection_datablock.name = coll.name
                collection_datablock.datablock = coll
            for obj in new_collection.all_objects:
                object_datablock = asset.object_datablocks.add()
                object_datablock.name = obj.name
                object_datablock.datablock = obj

        if asset.import_type == 'PROXY':
            # Cleanup asset
            asset.instance_object = None
            # asset.proxy_object = None
            asset.import_type = 'OVERRIDE'

        if self.make_local:
            # Make objects local as well
            asset.import_type = 'LOCAL'
            for coll in asset.collection_datablocks:
                coll.datablock.hide_select = False
                coll.datablock.hide_viewport = False
            for obj in asset.object_datablocks:
                obj.datablock.hide_select = False
                obj.datablock.select_set(True)
            bpy.ops.object.make_local(type='SELECT_OBDATA_MATERIAL')
            for coll in asset.collection_datablocks:
                if "_deform" in coll.name or "_cs" in coll.name or "_high" in coll.name or "_low" in coll.name:
                    coll.datablock.hide_select = True
                if "_deform" in coll.name or "_cs" in coll.name:
                    coll.datablock.hide_viewport = True
            asset.resolution = asset.resolution

        return {'FINISHED'}


class LFSSceneBuilderRemoveAsset(bpy.types.Operator):
    """Remove asset from scene"""
    bl_idname = "pipeline.scene_builder_remove_asset"
    bl_label = "Remove Asset"
    bl_options = {'REGISTER', 'UNDO'}

    asset_name: StringProperty(name="Asset Name", description="Name of the asset to remove")

    @staticmethod
    def remove_proxy_asset(asset):
        if asset.instance_object is not None:
            bpy.data.objects.remove(asset.instance_object)
        if asset.proxy_object is not None:
            bpy.data.objects.remove(asset.proxy_object)

    @staticmethod
    def remove_overridden_asset(asset):
        for obj in asset.object_datablocks:
            if obj.datablock is not None:
                bpy.data.objects.remove(obj.datablock)
        for coll in asset.collection_datablocks:
            if coll.datablock is not None:
                bpy.data.collections.remove(coll.datablock)

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)

    def execute(self, context):
        asset = context.scene.lfs_scene_builder_props.imported_assets[self.asset_name]

        if asset.import_type == 'PROXY':
            self.remove_proxy_asset(asset)
        elif asset.import_type == 'OVERRIDE':
            self.remove_overridden_asset(asset)

        index = context.scene.lfs_scene_builder_props.imported_assets.find(asset.name)
        context.scene.lfs_scene_builder_props.imported_assets.remove(index)
        return {'FINISHED'}


def draw_asset_selection(data, context, layout):
    addon_prefs = context.preferences.addons[__package__].preferences
    scene_props = context.scene.lfs_scene_builder_props

    if addon_prefs.root_path and os.path.isdir(addon_prefs.root_path):
        layout.prop_search(data, "asset_path", scene_props, "asset_paths")

        if not data.asset_path:
            return

        filepath = os.path.join(ROOT_PATH[0], data.asset_path, "rigging", "rig_ok_blend")
        
        if not os.path.isdir(filepath):
            layout.label(icon="ERROR", text="File not found")
            return

        try:
            # Get latest version
            filepath = os.path.join(filepath, sorted(os.listdir(filepath))[-1])
            # Get last file
            filepath = os.path.join(filepath, [p for p in os.listdir(filepath)
                                               if p.endswith('.blend')][-1])
            return filepath
        except (IndexError, FileNotFoundError):
            layout.label(icon="ERROR", text="File not found")
            return


class LFSSceneBuilderCopyLibreflowPath(bpy.types.Operator):
    """Copy Libreflow path to clipboard"""
    bl_idname = "pipeline.scene_builder_copy_libreflow_path"
    bl_label = "Copy Libreflow Path"
    bl_options = {'REGISTER'}

    asset_name: StringProperty(name="Asset Name", description="Name of the asset to remove")

    def execute(self, context):
        asset = context.scene.lfs_scene_builder_props.imported_assets[self.asset_name]
        lib_path = asset.library.filepath
        if not lib_path:
            return {'CANCELLED'}
        lib_path = os.path.abspath(bpy.path.abspath(lib_path)).replace("\\", "/")

        m = re.match(r".*/lib/([a-z0-9_]+)/([a-z0-9_]+)/([a-z0-9_]+)", lib_path)
        if m is None:
            return {'CANCELLED'}

        type, family, name = m.groups()
        context.window_manager.clipboard = f"/siren/asset_lib/asset_types/{type}/asset_families/{family}/assets/{name}"
        return {'FINISHED'}


class LFSSceneBuilderOpenAssetFile(bpy.types.Operator):
    """Open asset file in new blender instance"""
    bl_idname = "pipeline.scene_builder_open_asset_file"
    bl_label = "Open Asset File"
    bl_options = {'REGISTER'}

    asset_name: StringProperty(name="Asset Name", description="Name of the asset to remove")

    def execute(self, context):
        asset = context.scene.lfs_scene_builder_props.imported_assets[self.asset_name]
        lib_path = asset.library.filepath
        if not lib_path:
            return {'CANCELLED'}
        lib_path = os.path.abspath(bpy.path.abspath(lib_path)).replace("\\", "/")
        import subprocess
        subprocess.Popen([bpy.app.binary_path, lib_path])

        return {'FINISHED'}


class PIPELINE_PT_lfs_scene_builder_assets_panel(bpy.types.Panel):
    bl_label = "Assets"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"
    bl_parent_id = "PIPELINE_PT_lfs_scene_builder_panel"

    @classmethod
    def poll(cls, context):
        addon_prefs = bpy.context.preferences.addons[__package__].preferences
        scene_props = context.scene.lfs_scene_builder_props

        return (addon_prefs.root_path
                and os.path.isdir(addon_prefs.root_path)
                or len(scene_props.imported_assets))

    def draw(self, context):
        layout = self.layout

        scene_props = context.scene.lfs_scene_builder_props

        col = layout.column(align=True)
        filepath = draw_asset_selection(scene_props, context, col)

        if filepath is not None:
            col.label(text=filepath)
            asset_type, asset_family, asset_name = scene_props.asset_path.split("/")
            col.separator()
            row = col.row()
            row.operator_context = 'INVOKE_DEFAULT'
            op = row.operator("pipeline.scene_builder_import_asset", text="Import Asset")
            op.filepath = filepath
            op.asset_name = asset_name
            op.target_collection = asset_type

        if len(scene_props.imported_assets):
            row = layout.row()
            op = row.operator("pipeline.scene_builder_cleanup_anim", icon='KEY_DEHLT')
            op.do_all_assets = True

            if any(ass.is_updatable for ass in scene_props.imported_assets):
                row = layout.row()
                row.alert = True
                op = row.operator("pipeline.scene_builder_update_asset", text="Update All Assets", icon='FILE_REFRESH')
                op.do_all_assets = True

        row = layout.row(align=True)
        row.prop(scene_props, 'show_selected_only')
        row.prop(scene_props, 'filter', icon='VIEWZOOM', text='')

        # Asset list
        for asset in sorted(scene_props.imported_assets, key=lambda a: a.name):
            if ((not scene_props.show_selected_only or is_asset_selected(asset))
                    and (scene_props.filter == "" or scene_props.filter in asset.name)):

                box = layout.box()
                col = box.column()

                # Basic info (asset_name, asset_type)
                row = col.row(align=True)
                row.alignment = 'CENTER'
                row.prop(asset, 'display_ui', icon='DISCLOSURE_TRI_DOWN' if
                         asset.display_ui else 'DISCLOSURE_TRI_RIGHT',
                         emboss=False, text="")
                row.label(text=asset.name,
                          icon='LIBRARY_DATA_OVERRIDE' if asset.import_type == 'OVERRIDE'
                               else 'LINK_BLEND' if asset.import_type == 'PROXY'
                               else 'IMPORT')
                if asset.library is not None:
                    version = re.findall("[/\\\\](v[0-9]+)[/\\\\]", asset.library.filepath)
                    if version:
                        row.label(text=version[0])

                # More details, shown when arrow is down
                if asset.display_ui:
                    if asset.library is not None:
                        split = col.split(factor=0.8, align=True)
                        split.label(text=f'File path: {asset.library.filepath}')
                        group = split.row(align=True)
                        op = group.operator("pipeline.scene_builder_copy_libreflow_path", icon='COPYDOWN', text='LF')
                        op.asset_name = asset.name
                        op = group.operator("pipeline.scene_builder_open_asset_file", icon='FILE_BLEND', text='')
                        op.asset_name = asset.name
                    if len(asset.collection_datablocks):
                        col.separator()
                        col.label(text='Collections:')
                        for collection in asset.collection_datablocks:
                            if collection.datablock is not None:
                                col.label(text=collection.name, icon='OUTLINER_COLLECTION')
                            else:
                                col.label(text="Collection deleted", icon='ERROR')

                    if len(asset.object_datablocks):
                        col.separator()
                        col.label(text='Objects:')
                        for obj in asset.object_datablocks:
                            split = col.split(align=True)
                            if obj.datablock is not None:
                                split.label(text=obj.datablock.name, icon='OBJECT_DATA')
                            else:
                                split.label(text="Object deleted", icon='ERROR')
                            split.label(text=f"(Original: {obj.name}")

                    if asset.instance_object is not None:
                        col.separator()
                        col.label(text='Instance Object:')
                        col.label(text=asset.instance_object.name, icon='OUTLINER_OB_GROUP_INSTANCE')

                    if asset.proxy_object is not None:
                        col.separator()
                        col.label(text='Proxy Object:')
                        col.label(text=asset.proxy_object.name, icon='OUTLINER_OB_ARMATURE')

                # Resolution
                if None not in get_resolution_collections(asset):
                    row = col.row(align=True)
                    row.prop(asset, 'resolution', expand=True)


                # Operations
                col = box.column(align=True)
                if asset.import_type in {'PROXY', 'OVERRIDE'}:
                    row = col.row(align=True)
                    row.operator_context = 'INVOKE_DEFAULT'
                    if asset.import_type == 'PROXY':
                        op = row.operator("pipeline.scene_builder_convert_to_override", icon='LIBRARY_DATA_OVERRIDE')
                        op.asset_name = asset.name
                        op.make_local = False
                    op = row.operator("pipeline.scene_builder_convert_to_override", text='Make Local', icon='IMPORT')
                    op.asset_name = asset.name
                    op.make_local = True

                if asset.import_type in {'PROXY', 'OVERRIDE'}:
                    row = col.row(align=True)
                    row.operator_context = 'INVOKE_DEFAULT'
                    sub = row.row(align=True)
                    sub.alert = asset.is_updatable
                    op = sub.operator("pipeline.scene_builder_update_asset", icon='FILE_REFRESH')
                    op.asset_name = asset.name
                    op.do_all_assets = False

                    op = row.operator("pipeline.scene_builder_replace_asset")
                    op.asset_name = asset.name

                row = col.row(align=True)
                row.operator_context = 'INVOKE_DEFAULT'
                op = row.operator("pipeline.scene_builder_remove_asset", icon='TRASH')
                op.asset_name = asset.name

                col = box.column(align=True)
                row = col.row(align=True)
                row.active = (asset.proxy_object is not None
                             and asset.proxy_object.animation_data is not None
                             and asset.proxy_object.animation_data.action is not None)
                op = row.operator("pipeline.scene_builder_cleanup_anim", icon='KEY_DEHLT')
                op.do_all_assets = False
                op.asset_name = asset.name


class ObjectDatablock(bpy.types.PropertyGroup):
    datablock: bpy.props.PointerProperty(type=bpy.types.Object, name="Datablock")
    name: bpy.props.StringProperty(name="Name", default='')

class CollectionDatablock(bpy.types.PropertyGroup):
    datablock: bpy.props.PointerProperty(type=bpy.types.Collection, name="Datablock")
    name: bpy.props.StringProperty(name="Name", default='')


def get_resolution_collections(asset):
    collections = None
    if asset.import_type == 'PROXY' and asset.instance_object is not None:
        instance_object = asset.instance_object
        if instance_object.instance_collection is not None:
            collections = instance_object.instance_collection.children
    else:
        collections = (db.datablock for db in asset.collection_datablocks)

    if collections is None:
        return None, None

    low_coll, high_coll = None, None
    for coll in collections:
        if coll is not None:
            if "_low" in coll.name:
                low_coll = coll
            elif "_high" in coll.name or "_geom" in coll.name:
                high_coll = coll
            else:
                continue
    return low_coll, high_coll


def update_resolution(asset, _context):
    low_coll, high_coll = get_resolution_collections(asset)

    if None in (low_coll, high_coll):
        return

    low_coll.hide_viewport = asset.resolution != 'LOW'
    low_coll.hide_render = asset.resolution != 'LOW'
    high_coll.hide_viewport = asset.resolution != 'HIGH'
    high_coll.hide_render = asset.resolution != 'HIGH'

    # Use glitch to force update...
    for coll in (low_coll, high_coll):
        obj = coll.objects[0]
        obj_visible = obj.hide_viewport
        obj.hide_viewport = obj_visible


def update_updatable(context):
    for asset in context.scene.lfs_scene_builder_props.imported_assets:
        asset.is_updatable = True

        versions = get_available_versions(asset)

        if versions:
            latest_version = versions[0]
        else:
            continue


        latest_version = re.findall("[/\\\\]v([0-9]+)[/\\\\]", latest_version)
        if latest_version:
            latest_version = int(latest_version[0])
        else:
            continue

        if asset.library is None:
            continue

        current_version = re.findall("[/\\\\]v([0-9]+)[/\\\\]", asset.library.filepath)
        if current_version:
            current_version = int(current_version[0])
        else:
            continue

        asset.is_updatable = current_version != latest_version


@persistent
def resolution_handler(_scene):
    for asset in bpy.context.scene.lfs_scene_builder_props.imported_assets:
        update_resolution(asset, bpy.context)

@persistent
def updatable_handler(_scene):
    update_updatable(bpy.context)


class ImportedAsset(bpy.types.PropertyGroup):
    import_type: bpy.props.EnumProperty(
        name="Import Type",
        items=(('PROXY', 'Link', 'Asset was linked into scene and its rig proxied'),
               ('OVERRIDE', 'Override', 'Asset was linked into scene and overridden'),
               ('LOCAL', 'Local', 'Asset was made local to the scene')),
        default='PROXY')
    name: bpy.props.StringProperty(name="Asset Name", default='')

    asset_family: bpy.props.StringProperty(name="Asset Family", default='')
    asset_type: bpy.props.StringProperty(name="Asset Type", default='')
    asset_name: bpy.props.StringProperty(name="Asset Name", default='')

    # Resolution switch
    resolution: bpy.props.EnumProperty(
        name="Resolution",
        items=(('HIGH', 'High Resolution', 'Switch to high resolution'),
               ('LOW', 'Low Resolution', 'Switch to low resolution')),
        default='HIGH',
        update=update_resolution)

    library: bpy.props.PointerProperty(type=bpy.types.Library, name="Library")

    # Props for proxy asset
    instance_object: bpy.props.PointerProperty(type=bpy.types.Object, name="Instance Object")
    proxy_object: bpy.props.PointerProperty(type=bpy.types.Object, name="Proxy Object")

    # Props for overridden asset
    object_datablocks: bpy.props.CollectionProperty(
        name="Object Datablocks", type=ObjectDatablock)
    collection_datablocks: bpy.props.CollectionProperty(
        name="Collection Datablocks", type=CollectionDatablock)

    display_ui: bpy.props.BoolProperty(name="Show UI", default=False)

    is_updatable: bpy.props.BoolProperty(name="Updatable", default=False)


classes = (LFSSceneBuilderImportAsset, LFSSceneBuilderUpdateAsset,
           LFSSceneBuilderReplaceAsset, LFSSceneBuilderConvertToOverride,
           LFSSceneBuilderRemoveAsset, LFSSceneBuilderCopyLibreflowPath,
           LFSSceneBuilderOpenAssetFile,
           PIPELINE_PT_lfs_scene_builder_assets_panel, ObjectDatablock,
           CollectionDatablock)

def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.app.handlers.load_post.append(resolution_handler)
    bpy.app.handlers.load_post.append(updatable_handler)


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
    bpy.app.handlers.load_post.remove(resolution_handler)
    bpy.app.handlers.load_post.remove(updatable_handler)
