import bpy
from bpy.app.handlers import persistent
import re

separator = "<ø>"

def reference_diff_constraints(src_arma, target_arma):
    
    target_arma.lfs_scene_builder_diffential_constraints.clear()

    for new_bone in target_arma.pose.bones:
        old_bone = src_arma.pose.bones.get(new_bone.name)
        if old_bone:
            old_constraints = set([c.name for c in old_bone.constraints])
            new_constraints = set([c.name for c in new_bone.constraints])
            differentials = new_constraints.difference(old_constraints)
            
            for c in differentials:
                diff = target_arma.lfs_scene_builder_diffential_constraints.add()
                diff.bone_name = new_bone.name
                diff.constraint_name = c
                diff.name = f"{new_bone.name}{separator}{c}"


def clear_all():
    for a in bpy.data.objects:
        a.lfs_scene_builder_diffential_constraints.clear()


def get_assessment_name():
    r1 = "sq[0-9]{2,}"
    r2 = "sc[0-9]{4,}"
    try:
        f_name = bpy.data.filepath.split('/')[-1]
    except:
        return "assessment.txt"

    m1 = re.search(r1, f_name)
    m2 = re.search(r2, f_name)
    if m1 and m2:
        return f"assessment_{m1[0]}_{m2[0]}.txt"
    else:
        return "assessment.txt"


@persistent
def remove_unlinked_refs(dummy):
    for obj in bpy.data.objects:
        if obj.type == "ARMATURE":
            if obj.lfs_scene_builder_ref_arma is not None:
                ref = obj.lfs_scene_builder_ref_arma
                if not (ref.name in bpy.context.scene.objects):
                    obj.lfs_scene_builder_ref_arma = None

            if obj.lfs_scene_builder_new_arma is not None:
                ref = obj.lfs_scene_builder_new_arma
                if not (ref.name in bpy.context.scene.objects):
                    obj.lfs_scene_builder_new_arma = None


class LFSSceneBuilderAddToAssessment(bpy.types.Operator):

    """ Adds broken constraints to assessment """

    bl_idname = "pipeline.scene_builder_add_to_assessment"
    bl_label = "Add To Assessment"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.object is not None and context.object.type == 'ARMATURE'


    def execute(self, context):
        assessment_name = get_assessment_name()

        text = bpy.data.texts.get(assessment_name)
        if text is None:
            text = bpy.data.texts.new(assessment_name)

        text.write(f"     {context.object.name}:\n")

        passed = False
        for bone in context.object.pose.bones:
            reported = []
            for c in bone.constraints:
                if (not c.is_valid) and (not c.is_proxy_local):
                    reported.append(c.name)

            # Condition just to adapt message to singular/plural
            if len(reported) == 1:
                text.write(f"- On bone {bone.name} constraint: {reported[0]} is broken.\n")
                passed = True
            elif len(reported) > 1:
                text.write(f"- On bone {bone.name} constraints: {', '.join(reported)} are broken.\n")
                passed = True

        if passed:
            text.write("\n\n")

        return {'FINISHED'}


class LFSSceneBuilderClearAssessment(bpy.types.Operator):

    """ Clear all assessment """

    bl_idname = "pipeline.scene_builder_clear_assessment"
    bl_label = "Clear Assessment"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.object is not None and context.object.type == 'ARMATURE'


    def execute(self, context):
        assessment_name = get_assessment_name()

        text = bpy.data.texts.get(assessment_name)
        if text is not None:
            text.clear()

        return {'FINISHED'}



# One instance by differential constraint
class LFSSceneBuilder_DifferentialConstraint(bpy.types.PropertyGroup):
    bone_name       : bpy.props.StringProperty() # Name of the bone that carries a differential constraint
    constraint_name : bpy.props.StringProperty() # Name of the concerned constraint from the bone
    name            : bpy.props.StringProperty() # Concatenate


class LFSSceneBuilderRefreshDiff(bpy.types.Operator):

    """ Refresh differential bone from a reference """

    bl_idname = "pipeline.scene_builder_refresh_diff"
    bl_label = "Refresh Differential Constraints"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.object is not None and context.object.type == 'ARMATURE'


    def execute(self, context):
        ref = context.object.lfs_scene_builder_ref_arma

        if ref is None:
            return {'CANCELLED'}

        if ref.name not in context.scene.objects:
            context.object.lfs_scene_builder_ref_arma = None
            return {'CANCELLED'}

        if ref.type != 'ARMATURE':
            return {'CANCELLED'}

        reference_diff_constraints(ref, context.object)

        return {'FINISHED'}


# Must be reseted when we press the toggle button from another bone
class ToggleBonesData():
    def __init__(self):
        self.new_armature = None
        self.old_armature = None
        self.bone_name    = None
        self.bone_old     = None
        self.bone_new     = None
        self.last_mode    = None
        self.safe         = False

        self.current      = None
        self.target       = None

        self.attributes = ['new_armature', 'old_armature', 'bone_name', 'bone_old', 'bone_new', 'current', 'target', 'last_mode']
        self.obligatory = ['new_armature', 'old_armature', 'bone_name', 'bone_old', 'bone_new']

    def swap(self, context):
        if (self.current is self.new_armature) or (self.current is None):
            self.current = self.old_armature
            self.target = self.bone_old
        else:
            self.current = self.new_armature
            self.target = self.bone_new

        context.view_layer.objects.active = self.current
        self.current.data.bones.active = self.target.bone

    def is_valid(self):
        return all([getattr(self, attr) is not None for attr in self.obligatory])

    def same(self, name):
        return (self.bone_name is not None) or (self.bone_name == name)

    def reset(self):
        if not self.safe:
            for attr in self.attributes:
                setattr(self, attr, None)
        self.safe = False


toggle_bones_data = ToggleBonesData()

@persistent
def reset_toggle_mode(dummy):
    global toggle_bones_data
    toggle_bones_data.reset()

@persistent
def sniffer_mode(dummy):
    global toggle_bones_data

    current = bpy.context.mode

    if (toggle_bones_data.last_mode is None) or (current != toggle_bones_data.last_mode):
        toggle_bones_data.reset()
        toggle_bones_data.last_mode = current

class LFSSceneBuilderEnterTogglePoses(bpy.types.Operator):

    """ Enters the toggle pose mode to check a bone and its original """

    bl_idname = "pipeline.scene_builder_enter_toggle_pose"
    bl_label = "Enter Toggle Pose Bones"

    bone_name: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        if context.object is None:
            return False
        c1 = context.object.lfs_scene_builder_ref_arma
        c2 = context.object.lfs_scene_builder_new_arma
        c  = c1 if c1 is not None else c2
        state = (c1 or c2) and not (c1 and c2) # One of them but not both
        type_check = c.type == 'ARMATURE'
        return (state and type_check)


    def execute(self, context):
        global toggle_bones_data

        toggle_bones_data.new_armature = context.object if context.object.lfs_scene_builder_ref_arma is not None else context.object.lfs_scene_builder_new_arma
        toggle_bones_data.old_armature = toggle_bones_data.new_armature.lfs_scene_builder_ref_arma if toggle_bones_data.new_armature.lfs_scene_builder_ref_arma is not None else toggle_bones_data.new_armature.lfs_scene_builder_new_arma
        toggle_bones_data.bone_name    = self.bone_name
        toggle_bones_data.bone_old     = toggle_bones_data.old_armature.pose.bones.get(self.bone_name)
        toggle_bones_data.bone_new     = toggle_bones_data.new_armature.pose.bones.get(self.bone_name)

        print(toggle_bones_data.new_armature, toggle_bones_data.old_armature)
        print(toggle_bones_data.bone_name)
        print(toggle_bones_data.bone_new, toggle_bones_data.bone_old)

        if not toggle_bones_data.is_valid():
            toggle_bones_data.reset()
            self.report({'WARNING'}, 'Bone missing from other armature')
            return {'CANCELLED'}

        try:
            # Yeah it's dirty
            toggle_bones_data.safe = True
            bpy.ops.object.mode_set(mode='OBJECT')
            bpy.ops.object.select_all(action='DESELECT')
            toggle_bones_data.new_armature.select_set(True)
            toggle_bones_data.old_armature.select_set(True)
            toggle_bones_data.safe = True
            bpy.ops.object.mode_set(mode='POSE')
            toggle_bones_data.swap(context)
            self.report({'INFO'}, f"Watching: {toggle_bones_data.current.name}")
        except:
            self.report({'WARNING'}, "Failed to swap view")
            toggle_bones_data.reset()
            return {'CANCELLED'}

        return {'FINISHED'}


class LFSSceneBuilderTogglePoses(bpy.types.Operator):

    """ Toggle pose mode to check a bone and its original """

    bl_idname = "pipeline.scene_builder_toggle_pose"
    bl_label = "Toggle Pose Bones"

    @classmethod
    def poll(cls, context):
        global toggle_bones_data
        return (context.object is not None) and (context.mode == 'POSE') and (context.object.type == 'ARMATURE') and (toggle_bones_data.is_valid())


    def execute(self, context):
        global toggle_bones_data

        toggle_bones_data.swap(context)

        return {'FINISHED'}


class LFSSceneBuilderToggleConstraint(bpy.types.Operator):
    
    """ Toggle a constraint from a bone """
    
    bl_idname = "pipeline.scene_builder_toggle_constraint"
    bl_label = "Toggle the constraint"
    bl_options = {'REGISTER', 'UNDO'}

    target: bpy.props.StringProperty()
    
    @classmethod
    def poll(cls, context):
        return context.object is not None and context.object.type == 'ARMATURE'

    def execute(self, context):
        diff = context.object.lfs_scene_builder_diffential_constraints.get(self.target)
        if diff is None:
            return {'CANCELLED'}
        
        bone_name = diff.bone_name
        constraint_name = diff.constraint_name
        bone = context.object.pose.bones.get(bone_name)
        
        if bone is None:
            return {'CANCELLED'}
        
        constraint = bone.constraints.get(constraint_name)
        
        if constraint is None:
            return {'CANCELLED'}
        
        constraint.mute = not constraint.mute
        
        return {'FINISHED'}
    
    
class LFSSceneBuilderDeleteConstraint(bpy.types.Operator):
    
    """ Deletes a constraint from a bone """
    
    bl_idname = "pipeline.scene_builder_delete_constraint"
    bl_label = "Delete the constraint"
    bl_options = {'REGISTER', 'UNDO'}

    target: bpy.props.StringProperty()
    
    @classmethod
    def poll(cls, context):
        return context.object is not None and context.object.type == 'ARMATURE'

    def execute(self, context):
        diff = context.object.lfs_scene_builder_diffential_constraints.get(self.target)
        if diff is None:
            return {'CANCELLED'}
        
        bone_name = diff.bone_name
        constraint_name = diff.constraint_name
        bone = context.object.pose.bones.get(bone_name)
        
        if bone is None:
            return {'CANCELLED'}
        
        constraint = bone.constraints.get(constraint_name)
        
        if constraint is None:
            return {'CANCELLED'}

        if not constraint.is_proxy_local:
            self.report({'WARNING'}, "Can't remove from proxy")
        
        bone.constraints.remove(constraint)
        context.object.lfs_scene_builder_diffential_constraints.remove(context.object.lfs_scene_builder_diffential_constraints.find(self.target))
        return {'FINISHED'}
    

class LFSSceneBuilderMakeActive(bpy.types.Operator):
    
    """ Make a bone the active one """
    
    bl_idname = "pipeline.scene_builder_make_active"
    bl_label = "Make Active Bone"

    target: bpy.props.StringProperty()

    def execute(self, context):
        bone = context.object.pose.bones.get(self.target)
        if bone is None:
            return {'CANCELLED'}
        context.object.data.bones.active = bone.bone
        return {'FINISHED'}


class LFSSceneBuilderDifferentialConstraintsPanel(bpy.types.Panel):
    
    """ Panel exposing differences of constraints between the original armature and the present one """
    
    bl_label = "Differential Constraints Panel"
    bl_idname = "OBJECT_PT_LFS_SceneBuilder_diff_constraints"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    
    @classmethod
    def poll(cls, context):
        return context.object is not None and context.object.type == 'ARMATURE'

    def draw(self, context):
        layout = self.layout
        obj = context.object

        row = layout.row()
        if context.object.lfs_scene_builder_ref_arma is not None:
            split = row.split(factor=0.09, align=True)
            split.operator("pipeline.scene_builder_refresh_diff", text="", icon='FILE_REFRESH')
            split.prop_search(context.object, "lfs_scene_builder_ref_arma", bpy.data, "objects", text="", icon='OUTLINER_OB_ARMATURE')
        else:
            row.prop_search(context.object, "lfs_scene_builder_ref_arma", bpy.data, "objects", text="", icon='OUTLINER_OB_ARMATURE')

        if toggle_bones_data.is_valid():
            row = layout.row()
            row.operator("pipeline.scene_builder_toggle_pose", text="Swap version", icon='GROUP_BONE')
        
        if len(obj.lfs_scene_builder_diffential_constraints) > 0:
            for diff in obj.lfs_scene_builder_diffential_constraints:
                bone_name = diff.bone_name
                constraint_name = diff.constraint_name
                bone = context.object.pose.bones.get(bone_name)
                if bone is None:
                    continue
                constraint = bone.constraints.get(constraint_name)
                if constraint is None:
                    continue
                
                row = layout.row()
                split = row.split(factor=0.08, align=True)
                split.operator("pipeline.scene_builder_make_active", text="", icon='BONE_DATA').target = diff.bone_name

                split = split.split(factor=0.09, align=True)
                split.operator("pipeline.scene_builder_delete_constraint", text="", icon='TRASH').target = diff.name

                split = split.split(factor=0.09, align=True)
                if (context.object.lfs_scene_builder_ref_arma is not None) or (context.object.lfs_scene_builder_new_arma is not None):
                    split.operator("pipeline.scene_builder_enter_toggle_pose", text="", icon='UV_SYNC_SELECT').bone_name = bone.name
                else:
                    split.label(text="", icon='BLANK1')
                
                split = split.split(factor=0.3, align=True)
                split.label(text=diff.bone_name)
                
                split = split.split(factor=0.85, align=True)
                split.label(text=diff.constraint_name)
                
                split = split.split(factor=0.93, align=True)
                ico_name = 'HIDE_ON' if constraint.mute else 'HIDE_OFF'
                split.operator("pipeline.scene_builder_toggle_constraint", text="", icon=ico_name).target = diff.name
            
        else:
            row = layout.row()
            row.label(icon='CHECKBOX_HLT', text="No difference in constraints found")

class LFSSceneBuilderBrokenConstraintsPanel(bpy.types.Panel):
    
    """ Panel exposing broken constraints of the armature """
    
    bl_label = "Broken Constraints Panel"
    bl_idname = "OBJECT_PT_LFS_SceneBuilder_broken_constraints"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"
    
    @classmethod
    def poll(cls, context):
        return context.object is not None and context.object.type == 'ARMATURE'

    def draw(self, context):
        layout = self.layout
        nb = 0
        
        for bone in context.object.pose.bones:
            for c in bone.constraints:
                if not c.is_valid:
                    nb += 1
                    row = layout.row()
                    
                    split = row.split(factor=0.08, align=True)
                    split.operator("pipeline.scene_builder_make_active", text="", icon='BONE_DATA').target = bone.name

                    split = split.split(factor=0.09, align=True)
                    split.operator("pipeline.scene_builder_delete_constraint", text="", icon='TRASH').target = f"{bone.name}{separator}{c.name}"

                    split = split.split(factor=0.1, align=True)
                    if (context.object.lfs_scene_builder_ref_arma is not None) or (context.object.lfs_scene_builder_new_arma is not None):
                        split.operator("pipeline.scene_builder_enter_toggle_pose", text="", icon='UV_SYNC_SELECT').bone_name = bone.name
                    else:
                        split.label(text="", icon='BLANK1')

                    split = split.split(factor=0.4, align=True)
                    split.label(text=bone.name)
                    
                    split = split.split(factor=0.93, align=True)
                    split.label(text=c.name)

                    if not c.is_proxy_local:
                        split.label(text="", icon='LOCKED')
        
        row = layout.row()

        if nb == 0:
            row.label(icon='CHECKBOX_HLT', text="No broken constraint found")
        else:
            split = row.split(factor=0.6, align=True)
            split.operator("pipeline.scene_builder_add_to_assessment", icon='GREASEPENCIL')
            split = split.split(align=True)
            split.operator("pipeline.scene_builder_clear_assessment", icon='CANCEL')
                    

def register():
    bpy.utils.register_class(LFSSceneBuilder_DifferentialConstraint)
    bpy.types.Object.lfs_scene_builder_diffential_constraints = bpy.props.CollectionProperty(type=LFSSceneBuilder_DifferentialConstraint, options={'LIBRARY_EDITABLE'}, override={'LIBRARY_OVERRIDABLE'})
    bpy.types.Object.lfs_scene_builder_ref_arma = bpy.props.PointerProperty(type=bpy.types.Object, options={'LIBRARY_EDITABLE'}, override={'LIBRARY_OVERRIDABLE'})
    bpy.types.Object.lfs_scene_builder_new_arma = bpy.props.PointerProperty(type=bpy.types.Object, options={'LIBRARY_EDITABLE'}, override={'LIBRARY_OVERRIDABLE'})
    bpy.utils.register_class(LFSSceneBuilderDeleteConstraint)
    bpy.utils.register_class(LFSSceneBuilderToggleConstraint)
    bpy.utils.register_class(LFSSceneBuilderMakeActive)
    bpy.utils.register_class(LFSSceneBuilderRefreshDiff)
    bpy.utils.register_class(LFSSceneBuilderEnterTogglePoses)
    bpy.utils.register_class(LFSSceneBuilderTogglePoses)
    bpy.utils.register_class(LFSSceneBuilderAddToAssessment)
    bpy.utils.register_class(LFSSceneBuilderClearAssessment)
    bpy.utils.register_class(LFSSceneBuilderDifferentialConstraintsPanel)
    bpy.utils.register_class(LFSSceneBuilderBrokenConstraintsPanel)
    bpy.app.handlers.depsgraph_update_post.append(sniffer_mode)
    bpy.app.handlers.save_pre.append(remove_unlinked_refs)
    bpy.app.handlers.load_post.append(reset_toggle_mode)


def unregister():
    bpy.app.handlers.load_post.remove(reset_toggle_mode)
    bpy.app.handlers.save_pre.remove(remove_unlinked_refs)
    bpy.app.handlers.depsgraph_update_post.remove(sniffer_mode)
    bpy.utils.unregister_class(LFSSceneBuilderBrokenConstraintsPanel)
    bpy.utils.unregister_class(LFSSceneBuilderTogglePoses)
    bpy.utils.unregister_class(LFSSceneBuilderEnterTogglePoses)
    bpy.utils.unregister_class(LFSSceneBuilderClearAssessment)
    bpy.utils.unregister_class(LFSSceneBuilderAddToAssessment)
    bpy.utils.unregister_class(LFSSceneBuilderDifferentialConstraintsPanel)
    bpy.utils.unregister_class(LFSSceneBuilderRefreshDiff)
    bpy.utils.unregister_class(LFSSceneBuilderMakeActive)
    bpy.utils.unregister_class(LFSSceneBuilderToggleConstraint)
    bpy.utils.unregister_class(LFSSceneBuilderDeleteConstraint)
    bpy.utils.unregister_class(LFSSceneBuilder_DifferentialConstraint)
    clear_all()
    del bpy.types.Object.lfs_scene_builder_diffential_constraints
    del bpy.types.Object.lfs_scene_builder_ref_arma
    del bpy.types.Object.lfs_scene_builder_new_arma


class ConstraintsManager():
    @classmethod
    def register(cls):
        register()

    @classmethod
    def unregister(cls):
        unregister()


if __name__ == "__main__":
    register()


