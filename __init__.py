# Copyright (C) 2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


bl_info = {
    "name": "LFS Scene Builder",
    "author": "Les Fées Spéciales (LFS)",
    "version": (1, 1, 0),
    "blender": (2, 91, 0),
    "location": "Properties: Scene > Scene Builder",
    "description": "Helpers for shot creation",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Pipeline",
    }

import bpy
from bpy.app.handlers import persistent
from bpy.props import StringProperty, BoolProperty
import os

from .constraints_manager import ConstraintsManager as c_manager
from .setup import (LFSSceneBuilderSetupScene, LFSSceneBuilderSetupAnimation)
from .assets import (ImportedAsset, ROOT_PATH)
from .sets import LFSSceneBuilderImportSet
from .ref import (LFSSceneBuilderImportAudio, LFSSceneBuilderImportStoryboard,
                  LFSSceneBuilderUpdateAudio, LFSSceneBuilderUpdateStoryboard)


from . import render_setup

operator_classes = (LFSSceneBuilderSetupScene, LFSSceneBuilderSetupAnimation,
                    LFSSceneBuilderImportSet,
                    LFSSceneBuilderImportAudio, LFSSceneBuilderUpdateAudio,
                    LFSSceneBuilderImportStoryboard, LFSSceneBuilderUpdateStoryboard)


class PIPELINE_MT_lfs_scene_builder_menu(bpy.types.Menu):
    bl_idname = "PIPELINE_MT_lfs_scene_builder_menu"
    bl_label = "Scene Builder"

    def draw(self, context):
        layout = self.layout
        for c in operator_classes:
            layout.operator(c.bl_idname)


def scene_builder_menu(self, context):
    self.layout.menu("PIPELINE_MT_lfs_scene_builder_menu", icon='IMPORT')


class PIPELINE_PT_lfs_scene_builder_panel(bpy.types.Panel):
    bl_idname = "PIPELINE_PT_lfs_scene_builder_panel"
    bl_label = "Scene Builder"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"

    def draw(self, context):
        layout = self.layout

        col = layout.column(align=True)
        col.operator("pipeline.scene_builder_setup")

        addon_prefs = context.preferences.addons[__package__].preferences
        if addon_prefs.show_confo:
            col.operator("pipeline.scene_builder_setup_animation")


class LFSSceneBuilderProps(bpy.types.PropertyGroup):
    imported_assets: bpy.props.CollectionProperty(
        name="Imported Items", type=ImportedAsset)

    asset_paths: bpy.props.CollectionProperty(name="Asset Paths", type=bpy.types.PropertyGroup)
    asset_path: bpy.props.StringProperty(name="Asset Path", default='')

    audio_filepath: bpy.props.StringProperty(name="Audio File Path", default="", subtype='FILE_PATH')
    storyboard_filepath: bpy.props.StringProperty(name="Storyboard File Path", default="", subtype='FILE_PATH')

    is_audio_updatable: bpy.props.BoolProperty(name="Is Audio Updatable", default=False)
    is_storyboard_updatable: bpy.props.BoolProperty(name="Is Storyboard Updatable", default=False)

    use_masks: bpy.props.BoolProperty(name="Use Masks", default=False)

    show_selected_only: bpy.props.BoolProperty(name="Selected Only", default=False)
    filter: bpy.props.StringProperty(
        name='Filter',
        options={'TEXTEDIT_UPDATE'})


@persistent
def update_asset_list_props(_dummy):
    """Handler to list assets on startup"""
    asset_paths = bpy.context.scene.lfs_scene_builder_props.asset_paths
    asset_paths.clear()
    try:
        for t in sorted(os.listdir(ROOT_PATH[0])):
            if t in {'chars', 'props'} and os.path.isdir(os.path.join(ROOT_PATH[0], t)):
                for f in sorted(os.listdir(os.path.join(ROOT_PATH[0], t))):
                    if not f.startswith('_') and os.path.isdir(os.path.join(ROOT_PATH[0], t, f)):
                        for d in sorted(os.listdir(os.path.join(ROOT_PATH[0], t, f))):
                            if not d.startswith('_') and os.path.isdir(os.path.join(ROOT_PATH[0], t, f, d)):
                                path = asset_paths.add()
                                path.name = os.path.join(t, f, d).replace("\\", "/")
    except FileNotFoundError:
        return


def root_update(addon_prefs, context):
    ROOT_PATH[0] = (os.environ.get("ROOT_PATH")
                 or addon_prefs.get("root_path", ""))
    if ROOT_PATH[0]:
        ROOT_PATH[0] = os.path.join(ROOT_PATH[0], 'lib')


class LFSSceneBuilderPreferences(bpy.types.AddonPreferences):
    bl_idname = __name__

    root_path: StringProperty(
        name="Root Path",
        description="Base path for the project",
        default="",
        subtype='DIR_PATH',
        update=root_update)
    show_confo: BoolProperty(name="Show Conformation Tools",
                                  description="Show buttons for conformation in viewport's LFS panel and scene properties",
                                  default=False)

    windows_comp_export_path: StringProperty(
        name="Windows Comp Export Path",
        description="Windows server path, for compositing export",
        default="",
        subtype='DIR_PATH')

    def draw(self, context):
        layout = self.layout
        col = layout.column(align=True)
        col.prop(self, "root_path")

        col = layout.column(align=True)
        col.prop(self, "show_confo")
        if self.show_confo:
            col.prop(self, "windows_comp_export_path")


classes = (LFSSceneBuilderPreferences,
           PIPELINE_MT_lfs_scene_builder_menu,
           ImportedAsset,
           LFSSceneBuilderProps) + operator_classes

def register():
    bpy.utils.register_class(PIPELINE_PT_lfs_scene_builder_panel)
    c_manager.register()
    setup.register()
    assets.register()
    ref.register()
    sets.register()
    render_setup.register()

    for c in classes:
        bpy.utils.register_class(c)

    bpy.types.Scene.lfs_scene_builder_props = bpy.props.PointerProperty(type=LFSSceneBuilderProps)

    bpy.types.TOPBAR_MT_file_context_menu.prepend(scene_builder_menu)

    if __name__ in bpy.context.preferences.addons:
        addon_prefs = bpy.context.preferences.addons[__name__].preferences
    else:
        addon_prefs = {}
    root_update(addon_prefs, bpy.context)

    bpy.app.handlers.load_post.append(update_asset_list_props)

def unregister():
    bpy.app.handlers.load_post.remove(update_asset_list_props)

    bpy.types.TOPBAR_MT_file_context_menu.remove(scene_builder_menu)

    del bpy.types.Scene.lfs_scene_builder_props

    for c in reversed(classes):
        bpy.utils.unregister_class(c)

    ref.unregister()
    sets.unregister()
    assets.unregister()
    setup.unregister()
    render_setup.unregister()
    c_manager.unregister()
    bpy.utils.unregister_class(PIPELINE_PT_lfs_scene_builder_panel)


if __name__ == "__main__":
    register()
