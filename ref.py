# Copyright (C) 2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import bpy
from bpy.props import StringProperty, BoolProperty
from bpy.app.handlers import persistent
import os
import re

from .utils import get_versions_from_filepath
from .assets import ROOT_PATH


class LFSSceneBuilderImportAudio(bpy.types.Operator):
    """Import audio file from asset server"""
    bl_idname = "pipeline.scene_builder_import_audio"
    bl_label = "Import Audio"
    bl_options = {'REGISTER', 'UNDO'}

    # File props
    filepath: StringProperty(name="File Path",
                             description="Filepath used for importing the file",
                             maxlen=1024, subtype='FILE_PATH', )
    filter_folder: BoolProperty(default=True, options={'HIDDEN', 'SKIP_SAVE'})
    filter_sound: BoolProperty(default=True, options={'HIDDEN', 'SKIP_SAVE'})

    def invoke(self, context, _event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        name = os.path.basename(self.filepath)
        name = os.path.splitext(name)[0]
        sequence = context.scene.sequence_editor.sequences.new_sound(name, self.filepath,
                                                                     0, context.scene.frame_start)
        sequence.frame_start = sequence.frame_start  # Avoid sequences overlapping
        return {'FINISHED'}


def find_misc_dir():
    # Find scene dir
    scene_match = re.match(r"^.*?sc[0-9]+", bpy.data.filepath)
    if scene_match is None:
        return None

    # Find misc dir
    search_path = scene_match.group()
    if "misc" in os.listdir(search_path):
        return os.path.join(search_path, "misc")


class LFSSceneBuilderUpdateAudio(bpy.types.Operator):
    """Update audio file from asset server"""
    bl_idname = "pipeline.scene_builder_update_audio"
    bl_label = "Update Audio"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return hasattr(context.scene.sequence_editor, 'sequences')

    def add_sequence(self, context, search_path, filename):
        filepath = os.path.join(search_path, filename)
        versions = get_versions_from_filepath(filepath, extension='wav', search_from_version_file=False)
        versions = [(v, os.path.basename(os.path.dirname(v)), v) for v in versions]
        if versions:
            channel_path = versions[0][0]
            name = os.path.basename(channel_path)
            name = os.path.splitext(name)[0]
            sequence = context.scene.sequence_editor.sequences.new_sound(name, channel_path,
                                                                         0, context.scene.frame_start)
            sequence.frame_start = sequence.frame_start  # Avoid sequences overlapping
            return sequence

    def execute(self, context):
        search_path = find_misc_dir()
        if search_path is None:
            self.report({'WARNING'}, "Couldn't find misc directory")
            return {'CANCELLED'}

        sequence = None

        # Try to add split audio if found
        new_sequences = []
        for channel in ("audio_voices_wav", "audio_effects_wav"):
            if channel in os.listdir(search_path):
                sequence = self.add_sequence(context, search_path, channel)
                if sequence is not None:
                    new_sequences.append(sequence)

        # Else, try to find combined audio (voices + effects)
        if not len(new_sequences) and "audio_wav" in os.listdir(search_path):
            sequence = self.add_sequence(context, search_path, "audio_wav")
            if sequence is not None:
                new_sequences.append(sequence)

        # Remove old sequences
        if len(new_sequences):
            for old_seq in context.scene.sequence_editor.sequences[:]:
                if old_seq not in new_sequences:
                    context.scene.sequence_editor.sequences.remove(old_seq)
            context.scene.lfs_scene_builder_props.is_audio_updatable = False
        else:
            self.report({'WARNING'}, "Couldn't find any audio sequence.")
        return {'FINISHED'}


class LFSSceneBuilderUpdateStoryboard(bpy.types.Operator):
    """Update storyboard file from asset server"""
    bl_idname = "pipeline.scene_builder_update_storyboard"
    bl_label = "Update Storyboard"
    bl_options = {'REGISTER', 'UNDO'}

    def storyboard_version_items(self, context):
        bg_image = context.scene.camera.data.background_images[0]

        filepath = None
        if bg_image is not None:
            image = bg_image.image or bg_image.clip
            if image is not None:
                filepath = image.filepath
                versions = get_versions_from_filepath(filepath)
                versions = [(v, os.path.basename(os.path.dirname(v)), v) for v in versions]
                if versions:
                    return versions

        # Could not find file from the current SB path, try looking in the misc dir
        if not versions:
            search_path = find_misc_dir()
            if search_path is None:
                return []
            if "board_mp4" in os.listdir(search_path):
                print(os.path.join(search_path, "board_mp4"))
                versions = get_versions_from_filepath(os.path.join(search_path, "board_mp4"),
                                                      search_from_version_file=False)
                versions = [(v, os.path.basename(os.path.dirname(v)), v) for v in versions]
        return versions

    filepath: StringProperty(name="File Path",
                             description="Filepath used for importing the file", default="",
                             maxlen=1024, subtype='FILE_PATH', options={'HIDDEN', 'SKIP_SAVE'})
    version: bpy.props.EnumProperty(name="Version", items=storyboard_version_items)

    @classmethod
    def poll(cls, context):
        cam = context.scene.camera
        return cam is not None and len(cam.data.background_images)

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def execute(self, context):
        if self.filepath:
            new_filepath = self.filepath
        else:
            new_filepath = self.version
            new_filepath = bpy.path.relpath(self.version)

        if context.scene.camera.data.background_images[0].image is not None:
            bg_image = context.scene.camera.data.background_images[0]
            bg_image.image.filepath = new_filepath
            bg_image.scale = 0.25
            bg_image.offset = (-0.375, -0.375)
            bg_image.alpha = 0.5
            context.scene.lfs_scene_builder_props.is_storyboard_updatable = False
        else:
            print(f"Image not found for camera {context.scene.camera.name}")
            return {'CANCELLED'}

        return {'FINISHED'}


class LFSSceneBuilderImportStoryboard(bpy.types.Operator):
    """Import storyboard from asset server.

    This adds the storyboard image as a background image in the scene
    camera.
    """
    bl_idname = "pipeline.scene_builder_import_storyboard"
    bl_label = "Import Storyboard"
    bl_options = {'REGISTER', 'UNDO'}

    use_corner: BoolProperty(default=True, description="Put image in corner, or keep full screen as default")

    filepath: StringProperty(name="File Path",
                             description="Filepath used for importing the file",
                             maxlen=1024, subtype='FILE_PATH', )
    filter_folder: BoolProperty(default=True, options={'HIDDEN', 'SKIP_SAVE'})
    filter_image: BoolProperty(default=True, options={'HIDDEN', 'SKIP_SAVE'})
    filter_movie: BoolProperty(default=True, options={'HIDDEN', 'SKIP_SAVE'})

    @classmethod
    def poll(cls, context):
        return context.scene.camera is not None and context.scene.camera.type == 'CAMERA'

    def invoke(self, context, _event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        if not os.path.isfile(self.filepath):
            return {'CANCELLED'}

        image = bpy.data.images.load(self.filepath, check_existing=True)
        context.scene.camera.data.show_background_images = True
        bg_image = context.scene.camera.data.background_images.new()
        bg_image.image = image
        bg_image.display_depth = 'FRONT'
        bg_image.frame_method = 'FIT'
        if self.use_corner:
            bg_image.scale = 0.25

        bg_image.image.update()
        bg_image.image_user.frame_duration = bg_image.image.frame_duration
        bg_image.image_user.frame_start = context.scene.frame_start

        if self.use_corner:
            scene_x = context.scene.render.resolution_x
            scene_y = context.scene.render.resolution_y
            image_x = image.size[0]
            image_y = image.size[1]

            # Fit in horizontal
            if scene_y / scene_x >= image_y / image_x:
                bg_image.offset.x = -0.5 + (bg_image.scale) / 2.0
                bg_image.offset.y = -(scene_y/scene_x) / (image_y/image_x) / 2.0 + (bg_image.scale) / 2.0
            else:
                pass
                # TODO WTF is this shit!?
                # bg_image.offset.x = -0.5 + (scene_y/scene_x) / (image_y/image_x) / 2.0
                # bg_image.offset.y = -(scene_y/scene_x) / (image_y/image_x) / 2.0 + (bg_image.scale) / 2.0

        return {'FINISHED'}


class PIPELINE_PT_lfs_scene_builder_ref_panel(bpy.types.Panel):
    bl_label = "Reference"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"
    bl_parent_id = "PIPELINE_PT_lfs_scene_builder_panel"

    def draw(self, context):
        layout = self.layout
        scene_props = context.scene.lfs_scene_builder_props

        col = layout.column(align=True)
        row = col.row(align=True)
        row.active = scene_props.is_audio_updatable

        if scene_props.is_audio_updatable:
            row.alert = True
        else:
            row.active = False
        row.operator("pipeline.scene_builder_update_audio")

        row = col.row(align=True)
        if scene_props.is_storyboard_updatable:
            row.alert = True
        else:
            row.active = False
        row.operator("pipeline.scene_builder_update_storyboard")


@persistent
def get_assets_updatable(_dummy):
    """Handler to check whether assets are updatable on file load"""
    if bpy.data.filepath and ROOT_PATH[0]:
        # Update audio sequence
        sequencer = bpy.context.scene.sequence_editor
        if hasattr(sequencer, 'sequences'):
            # Get first audio sequence in sequencer
            found_sequence = False
            for sequence in sequencer.sequences:
                if sequence.type == 'SOUND':
                    found_sequence = True
                    break

            if found_sequence:
                filepath = sequence.sound.filepath
                filepath = bpy.path.relpath(filepath)
                versions = get_versions_from_filepath(filepath)
                if not versions:
                    bpy.context.scene.lfs_scene_builder_props.is_audio_updatable = True
                else:
                    new_filepath = versions[0]
                    if filepath != new_filepath:
                        bpy.context.scene.lfs_scene_builder_props.is_audio_updatable = True
                    else:
                        bpy.context.scene.lfs_scene_builder_props.is_audio_updatable = False
        else:
            bpy.context.scene.lfs_scene_builder_props.is_audio_updatable = False

        # Update storyboard sequence
        cam = bpy.context.scene.camera
        if (cam is not None
                and len(cam.data.background_images)
                and cam.data.background_images[0].image is not None):
            filepath = cam.data.background_images[0].image.filepath
            filepath = bpy.path.relpath(filepath)
            versions = get_versions_from_filepath(filepath)
            if not versions:
                bpy.context.scene.lfs_scene_builder_props.is_storyboard_updatable = True
            else:
                new_filepath = versions[0]

                if filepath != new_filepath:
                    bpy.context.scene.lfs_scene_builder_props.is_storyboard_updatable = True
        else:
            bpy.context.scene.lfs_scene_builder_props.is_storyboard_updatable = False

classes = (PIPELINE_PT_lfs_scene_builder_ref_panel,)

def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.app.handlers.load_post.append(get_assets_updatable)

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
    bpy.app.handlers.load_post.remove(get_assets_updatable)
