# Copyright (C) 2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import bpy
from mathutils import Vector
import os
import re


def create_shadow_catcher_material(name):
    """Create a material and node tree for the shadow catcher"""
    material = bpy.data.materials.new(name)
    material.use_nodes = True

    material.blend_method = 'BLEND'

    # Remove nodes from active node tree
    node_tree = material.node_tree
    for node in node_tree.nodes:
        node_tree.nodes.remove(node)

    # Create nodes
    node = node_tree.nodes.new("ShaderNodeOutputMaterial")
    node.location = Vector((420.0, 200.0))
    node.name = 'Material Output'
    node.inputs['Displacement'].default_value = (0.0, 0.0, 0.0)

    node = node_tree.nodes.new("ShaderNodeBsdfDiffuse")
    node.location = Vector((-60.0, 120.0))
    node.name = 'Diffuse BSDF.001'
    node.inputs['Color'].default_value = (0.0, 0.0, 0.0, 1.0)
    node.inputs['Roughness'].default_value = 0.0
    node.inputs['Normal'].default_value = (0.0, 0.0, 0.0)

    node = node_tree.nodes.new("ShaderNodeBsdfTransparent")
    node.location = Vector((-60.0, -20.0))
    node.name = 'Transparent BSDF'
    node.inputs['Color'].default_value = (1.0, 1.0, 1.0, 1.0)

    node = node_tree.nodes.new("ShaderNodeShaderToRGB")
    node.location = Vector((-760.0, 200.0))
    node.name = 'Shader to RGB'

    node = node_tree.nodes.new("ShaderNodeBsdfDiffuse")
    node.location = Vector((-980.0, 200.0))
    node.name = 'Diffuse BSDF'
    node.inputs['Color'].default_value = (1.0, 1.0, 1.0, 1.0)
    node.inputs['Roughness'].default_value = 0.0
    node.inputs['Normal'].default_value = (0.0, 0.0, 0.0)

    node = node_tree.nodes.new("ShaderNodeRGBToBW")
    node.location = Vector((-540.0, 200.0))
    node.name = 'RGB to BW.001'
    node.inputs['Color'].default_value = (0.5, 0.5, 0.5, 1.0)

    node = node_tree.nodes.new("ShaderNodeClamp")
    node.clamp_type = 'MINMAX'
    node.location = Vector((-320.0, 200.0))
    node.name = 'Clamp'
    node.inputs['Value'].default_value = 1.0
    node.inputs['Min'].default_value = 0.0
    node.inputs['Max'].default_value = 1.0

    node = node_tree.nodes.new("ShaderNodeMixShader")
    node.location = Vector((220.0, 200.0))
    node.name = 'Mix Shader'
    node.inputs['Fac'].default_value = 0.5

    node_tree.links.new(node_tree.nodes['Shader to RGB'].inputs[0],
                        node_tree.nodes['Diffuse BSDF'].outputs[0])

    node_tree.links.new(node_tree.nodes['RGB to BW.001'].inputs[0],
                        node_tree.nodes['Shader to RGB'].outputs[0])

    node_tree.links.new(node_tree.nodes['Mix Shader'].inputs[1],
                        node_tree.nodes['Diffuse BSDF.001'].outputs[0])

    node_tree.links.new(node_tree.nodes['Mix Shader'].inputs[2],
                        node_tree.nodes['Transparent BSDF'].outputs[0])

    node_tree.links.new(node_tree.nodes['Clamp'].inputs[0],
                        node_tree.nodes['RGB to BW.001'].outputs[0])

    node_tree.links.new(node_tree.nodes['Material Output'].inputs[0],
                        node_tree.nodes['Mix Shader'].outputs[0])

    node_tree.links.new(node_tree.nodes['Mix Shader'].inputs[0],
                        node_tree.nodes['Clamp'].outputs[0])

    return material


def get_versions_from_filepath(filepath, extension='', search_from_version_file=True):
    """Assuming the files are stored on disk in the form:
           ./v00*/file.ext
    Go up twice from the file path, return list of version dirs and the first
    file found in them.
    """
    dirname = os.path.abspath(bpy.path.abspath(filepath))

    if search_from_version_file:
        dirname = os.path.dirname(dirname)
        dirname = os.path.dirname(dirname)

    if not os.path.exists(dirname):
        return []

    versions = []
    for version in sorted(os.listdir(dirname), reverse=True):
        if re.match("^v[0-9]+$", version):
            version = os.path.join(dirname, version)
            files = [f for f in os.listdir(version) if f.endswith(extension)]
            if not files:
                continue
            version = bpy.path.relpath(os.path.join(version, files[-1]))
            versions.append(version)
    return versions


def assign_object_to_collection(obj, coll_name):
    """Assign obj to a collection called coll_name, creating it if necessary."""
    coll = bpy.data.collections.get(coll_name)
    if coll is None:
        coll = bpy.data.collections.new(coll_name)
    for former_coll in obj.users_collection:
        former_coll.objects.unlink(obj)
    coll.objects.link(obj)
    if coll.name not in bpy.context.scene.collection.children:
        bpy.context.scene.collection.children.link(coll)
    return coll


def assign_collection_to_collection(coll, dst_coll_name):
    """Assign obj to a collection called coll_name, creating it if necessary."""
    dst_coll = bpy.data.collections.get(dst_coll_name)
    if dst_coll is None:
        dst_coll = bpy.data.collections.new(dst_coll_name)
    # Unlink collection from all collection
    for parent_coll in traverse_tree(bpy.context.scene.collection):
        if coll.name in parent_coll.children:
            parent_coll.children.unlink(coll)
    dst_coll.children.link(coll)
    return dst_coll

def traverse_tree(t):
    """Get all children recursively.

    From https://blender.stackexchange.com/a/167889/4979
    """
    yield t
    for child in t.children:
        yield from traverse_tree(child)


def get_child_recursively(child_name, tree):
    """Get a child by recursively traversing tree. Return child if found, else None """
    child = None
    for c in traverse_tree(tree):
        if c.name == child_name:
            child = c
            break
    return child


def relink_constraints(old_obj, new_obj):
    def copy_attributes(old_constraint, old_obj, new_constraint, new_obj):
        """Try to copy multiple constraint attributes"""
        if (hasattr(old_constraint, 'target')
                and old_constraint.target is old_obj):
            new_constraint.target = new_obj
        if (hasattr(old_constraint, 'subtarget')
                and old_constraint.target is old_obj):
            new_constraint.subtarget = old_constraint.subtarget
        if hasattr(old_constraint, 'inverse_matrix'):
            new_constraint.inverse_matrix = old_constraint.inverse_matrix
        new_constraint.mute = old_constraint.mute

    for old_constraint in old_obj.constraints:
        if not old_constraint.name in new_obj.constraints:
            new_constraint = new_obj.constraints.copy(old_constraint)
        else:
            new_constraint = new_obj.constraints[old_constraint.name]
        copy_attributes(old_constraint, old_obj, new_constraint, new_obj)

    # Update bone constraints
    for old_pbone in old_obj.pose.bones:
        if old_pbone.name in new_obj.pose.bones:
            new_pbone = new_obj.pose.bones[old_pbone.name]
            for old_constraint in old_pbone.constraints:
                if not old_constraint.name in new_pbone.constraints:
                    new_constraint = new_pbone.constraints.copy(old_constraint)
                else:
                    new_constraint = new_pbone.constraints[old_constraint.name]
                copy_attributes(old_constraint, old_obj, new_constraint, new_obj)

    # Update constraints from other objects
    for other_object in bpy.context.scene.objects:
        if other_object not in {old_obj, new_obj}:
            for constraint in other_object.constraints:
                if (hasattr(constraint, 'target')
                        and constraint.target is old_obj):
                    constraint.target = new_obj

            # Update bone constraints from other objets
            if other_object.type == 'ARMATURE':
                for other_pbone in other_object.pose.bones:
                    for constraint in other_pbone.constraints:
                        if (hasattr(constraint, 'target')
                               and constraint.target is old_obj):
                            constraint.target = new_obj


def reset_bone_controllers(bone):
    """Borrowed from rig tools"""
    for key in bone.keys():
        if 'ik_fk_switch' in key:
            if 'hand' in bone.name:
                bone['ik_fk_switch'] = 1.0
            else:
                bone['ik_fk_switch'] = 0.0
        if 'fly_torso_root' in key:
            bone['world_fly_root_torso_head']=3
        #if 'ik_fk_spine' in key:
        #    bone['ik_fk_spine']=0
        if 'stretch_length' in key:
            bone['stretch_length'] = 1.0
        if 'copy_arm_y_rot' in key:
            bone['copy_arm_y_rot'] = 1.0
        if 'auto_stretch' in key:
            bone['auto_stretch'] = 0.0
        if 'auto_transform' in key and 'shirt' not in bone.name and 'apron' not in bone.name:
            bone['auto_transform'] = 0.0
        if 'pin' in key:
            if 'leg' in key:
                bone['leg_pin'] = 0.0
            else:
                bone['elbow_pin'] = 0.0
        if 'bend_all' in key:
            bone['bend_all'] = 0.0
        if 'pole_parent' in key:
            bone['pole_parent'] = 1
        if 'fingers_grasp' in key:
            bone['fingers_grasp'] = 0.0
        if "arm_spine_rot" in key:
            bone['arm_spine_rot'] = 1.0
        if "toes_pivot" in key:
            bone['toes_pivot'] = 0.0
        if "toes_x" in key:
            bone['toes_x'] = 0.0
        if "toes_y" in key:
            bone['toes_y'] = 0.0
        if "toes_z" in key:
            bone['toes_z'] = 0.0
        if "sandal_bend" in key:
            bone["sandal_bend"]=0.0
        if "foot_roll_z" in key:
            bone["foot_roll_z"]=0.0
        if "foot_roll_x" in key:
            bone["foot_roll_x"]=0.0
        if "foot_rot" in key:
            bone["foot_rot"]=0.0
        if "fly_root_parent" in key:
            bone["fly_root_parent"]=0
        if "hand_rot_copy" in key:
            bone["hand_rot_copy"]=0.0
        if "hide_spec" in key:
            bone["hide_spec"]=0
        if "spec_target" in key:
            bone["spec_target"]=0.0
        if "world_fly_root_torso_head" in key:
            bone["world_fly_root_torso_head"]=2


def project_3d_point(camera: bpy.types.Object,
                     p: Vector,
                     render: bpy.types.RenderSettings) -> Vector:
    """
    From https://blender.stackexchange.com/a/86570

    Given a camera and its projection matrix M;
    given p, a 3d point to project:

    Compute P’ = M * P
    P’= (x’, y’, z’, w')

    Ignore z'
    Normalize in:
    x’’ = x’ / w’
    y’’ = y’ / w’

    x’’ is the screen coordinate in normalised range -1 (left) +1 (right)
    y’’ is the screen coordinate in  normalised range -1 (bottom) +1 (top)

    :param camera: The camera for which we want the projection
    :param p: The 3D point to project
    :param render: The render settings associated to the scene.
    :return: The 2D projected point in normalized range [-1, 1] (left to right, bottom to top)
    """

    if camera.type != 'CAMERA':
        raise Exception("Object {} is not a camera.".format(camera.name))

    if len(p) != 3:
        raise Exception("Vector {} is not three-dimensional".format(p))

    # Get the two components to calculate M
    modelview_matrix = camera.matrix_world.inverted()
    projection_matrix = camera.calc_matrix_camera(
        bpy.context.view_layer.depsgraph,
        x = render.resolution_x,
        y = render.resolution_y,
        scale_x = render.pixel_aspect_x,
        scale_y = render.pixel_aspect_y,
    )

    # print(projection_matrix * modelview_matrix)

    # Compute P’ = M * P
    p1 = projection_matrix @ modelview_matrix @ Vector((p.x, p.y, p.z, 1))

    # Normalize in: x’’ = x’ / w’, y’’ = y’ / w’
    p2 = Vector(((p1.x/p1.w, p1.y/p1.w)))

    return p2


def cleanup_windows():
    while len(bpy.context.window_manager.windows) > 1:
        context = bpy.context.copy()
        context['window'] = bpy.context.window_manager.windows[-1]
        bpy.ops.wm.window_close(context)


def convert_assets_to_overrides():
    """Convert proxy assets to overrides"""
    asset_objects = []
    for type in ('Chars', 'Props'):
        if type in bpy.data.collections:
            asset_objects.extend(bpy.data.collections[type].all_objects)

    for asset in bpy.context.scene.lfs_scene_builder_props.imported_assets:
        if asset.import_type == 'PROXY':
            proxy_object = asset.proxy_object
            if proxy_object is not None and proxy_object in asset_objects:
                bpy.ops.pipeline.scene_builder_convert_to_override(asset_name=asset.name)
