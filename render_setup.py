# Copyright (C) 2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import bpy
from bpy.app.handlers import persistent
from bpy_extras.io_utils import ExportHelper
from mathutils import Color, Vector
import addon_utils
import os
import re
from random import uniform

from .utils import (traverse_tree, get_child_recursively,
                    get_versions_from_filepath, project_3d_point,
                    cleanup_windows, convert_assets_to_overrides,
                    assign_object_to_collection, assign_collection_to_collection)

from .setup import sort_collections

# TODO process shadow catcher if visible and not excluded
RENDERED_COLLECTIONS = ['Chars', 'Props', 'Grease_Pencil', 'Shadow_catcher']
HIDDEN_COLLECTIONS = ['Ref', 'Refs', 'Sets', 'Widgets']


def reset_view_layers():
    """Deletes all existing view layers and resets the remaining one to get a fresh start"""
    while (len(bpy.context.scene.view_layers) > 1):
        vl = bpy.context.scene.view_layers[-1]
        bpy.context.scene.view_layers.remove(vl)
    bpy.context.scene.view_layers[0].name = "View Layer"

    root = bpy.context.scene.view_layers[0].layer_collection
    for collection in traverse_tree(root):
        collection.holdout = False
        collection.exclude = False


def create_node_tree(context, base_path):
    # Setup scene to use nodes
    context.scene.use_nodes = True
    node_tree = context.scene.node_tree
    cam = context.scene.camera

    # Clear present nodes
    node_tree.nodes.clear()

    # Add a layer for each view layer

    last = 0
    spacing = 100

    for i, layer in enumerate(context.scene.view_layers):
        n = context.scene.node_tree.nodes.new(type='CompositorNodeRLayers')
        n.location = (0, last)
        n.layer = layer.name
        last -= n.height * 4 + spacing

    output_node = context.scene.node_tree.nodes.new(type='CompositorNodeOutputFile')
    output_node.location = (1450, 0)
    output_node.width = 500
    output_node.base_path = base_path
    output_node.format.file_format = 'PNG'
    output_node.format.color_mode = 'RGBA'
    output_node.format.color_depth = '8'
    output_node.format.compression = 15

    output_node.inputs.clear()

    nodes = context.scene.node_tree.nodes
    for layer_node in context.scene.node_tree.nodes:
        if layer_node.type != 'R_LAYERS':
            continue
        layer_name = layer_node.layer
        layer = context.scene.view_layers[layer_name]
        if layer_name == 'View Layer':
            layer_name = '000_' + layer_name

        # Frame
        frame = context.scene.node_tree.nodes.new("NodeFrame")
        frame.label = layer_name
        frame.label_size = 64
        frame.use_custom_color = True
        frame_color = Color()
        frame_color.v = 0.2
        frame_color.s = 0.5
        frame_color.h = uniform(0.0, 1.0)
        frame.color = frame_color
        layer_node.parent = frame

        pass_index = 1
        for pass_output in layer_node.outputs:
            if pass_output.enabled and (pass_output.name not in {'Alpha', 'Depth'}
                                   and not pass_output.name.startswith('Crypto')):

                name_folder = f"{layer_name}_{pass_index:02}_{pass_output.name}"
                name_folder = bpy.path.clean_name(name_folder)

                name = name_folder + '/' + name_folder + '.'
                file_input = output_node.file_slots.new(name)

                # Convert float to RGB
                if pass_output.type == 'VALUE':
                    map_range_node = context.scene.node_tree.nodes.new(type='CompositorNodeMapRange')
                    map_range_node.parent = frame
                    map_range_node.location = layer_node.location
                    map_range_node.location.x += 270.0

                    converter_node = context.scene.node_tree.nodes.new(type='CompositorNodeSetAlpha')
                    converter_node.parent = frame
                    converter_node.location = layer_node.location
                    converter_node.location.x += 440.0

                    node_tree.links.new(pass_output, map_range_node.inputs['Value'])
                    node_tree.links.new(map_range_node.outputs['Value'], converter_node.inputs['Image'])

                    pass_output = converter_node.outputs['Image']

                    file_slot = output_node.file_slots[file_input.name]
                    file_slot.use_node_format = False
                    file_slot.format.file_format = 'PNG'
                    file_slot.format.color_mode = 'RGBA'
                    file_slot.format.color_depth = '16'


                node_tree.links.new(pass_output, file_input)


        # Special setup when using cryptomatte masks

        # Get collection
        collection_rexp = r"[0-9]{3}_(.*)"
        collection_match = re.match(collection_rexp, layer_node.layer)
        if collection_match is not None:
            collection_name = collection_match.groups()[0]
            collection = bpy.data.collections.get(collection_name)
            if collection is not None and len(collection.children) > 1:
                layer.use_pass_cryptomatte_object = True

                # Get asset centers
                asset_centers = {}
                for child_coll in collection.children:
                    if collection is not None:
                        for child_coll in collection.children:
                            arm_obj = None
                            for obj in child_coll.all_objects:
                                if obj.type == 'ARMATURE':
                                    arm_obj = obj
                                    break
                            if arm_obj is not None and "c_traj" in arm_obj.pose.bones:
                                traj_loc = arm_obj.matrix_world @ arm_obj.pose.bones["c_traj"].matrix.to_translation()
                                asset_centers[child_coll] = traj_loc

                            else:
                                # Could not find armature, choose first mesh we find instead
                                mesh_obj = None
                                for obj in child_coll.all_objects:
                                    if obj.type == 'MESH':
                                        mesh_obj = obj
                                        break
                                if mesh_obj is not None:
                                    # https://blenderartists.org/t/calculating-bounding-box-center-coordinates/546894/3
                                    loc = mesh_obj.matrix_world @ (sum((Vector(b) for b in mesh_obj.bound_box), Vector()) / 8)
                                    asset_centers[child_coll] = loc

                # Sort assets from left to right in camera view
                colls = sorted(asset_centers, key=lambda coll: project_3d_point(cam, asset_centers[coll], context.scene.render).x)

                for c_i, child_coll in enumerate(colls):

                    # Create combine node and connect it to file output
                    if c_i % 3 == 0:
                        combine_node = node_tree.nodes.new('CompositorNodeCombRGBA')
                        combine_node.parent = frame
                        combine_node.location = (1000, layer_node.location.y - ((c_i // 3) * 3) * 250)
                        layer_input = output_node.layer_slots.new(f"{layer.name}_{(c_i // 3 + 2):02d}_Matte/{layer.name}_{(c_i // 3 + 2):02d}_Matte.")
                        node_tree.links.new(layer_input, combine_node.outputs["Image"])

                    objs = []
                    for obj in child_coll.all_objects:
                        if obj.hide_render:
                            continue
                        if obj.type == 'MESH':
                            objs.append(obj)
                        elif obj.type == 'EMPTY' and obj.instance_collection is not None:
                            for sub in obj.instance_collection.all_objects:
                                if sub.type == 'MESH' and (not sub.name.endswith("_low")
                                                        and not sub.name.startswith("cs_")):
                                    objs.append(sub)

                    obj_names = sorted((o.name for o in objs))

                    crypto_node = node_tree.nodes.new('CompositorNodeCryptomatteV2')
                    crypto_node.parent = frame
                    crypto_node.label = child_coll.name
                    crypto_node.location = (632, layer_node.location.y + c_i * -250)
                    crypto_node.source == 'RENDER'
                    crypto_node.scene = context.scene
                    crypto_node.layer_name = f'{layer.name}.CryptoObject'
                    crypto_node.matte_id = ','.join(obj_names)
                    for node_output in crypto_node.outputs:
                        if node_output.name != 'Matte':
                            node_output.hide = True

                    node_tree.links.new(combine_node.inputs[c_i % 3], crypto_node.outputs["Matte"])

    # Might be a heavy operation depending on the scene, moved to the end of the function
    context.scene.render.use_simplify = False


class LFSSceneBuilderSetupRender(bpy.types.Operator):
    """Setup render scene from animation scene"""
    bl_idname = "pipeline.scene_builder_setup_render"
    bl_label = "Setup Render Scene"
    bl_options = {'REGISTER', 'UNDO'}

    kitsu_duration: bpy.props.IntProperty(default=-1)

    def execute(self, context):
        # Cleanup windows
        cleanup_windows()

        # Setup stuff in scene
        context.scene.tool_settings.use_keyframe_insert_auto = False

        context.scene.render.use_compositing = True
        context.scene.use_nodes = True
        context.scene.render.use_file_extension = True

        context.scene.eevee.taa_render_samples = 16

        # Store duration of shot from Kitsu
        if self.kitsu_duration != -1:
            context.scene['kitsu_duration'] = self.kitsu_duration

        # High res for each asset
        for asset in context.scene.lfs_scene_builder_props.imported_assets:
            asset.resolution = "HIGH"

        # # Use cam_render
        # if ("Camera_render" in context.scene.objects
        #         and context.scene.camera != context.scene.objects["Camera_render"]):
        #     context.scene.camera = context.scene.objects["Camera_render"]

        camera = context.scene.camera
        if camera is not None:
            # Hide BG image
            for bg_image in camera.data.background_images:
                if bg_image.source == 'IMAGE':
                    if bg_image.image is not None and "misc_board" in bg_image.image.filepath:
                        bg_image.show_background_image = False
                        bg_image.show_expanded = False
                else:
                    if bg_image.clip is not None and "misc_board" in bg_image.clip.filepath:
                        bg_image.show_background_image = False
                        bg_image.show_expanded = False
            camera.data.show_mist = True

        # Del ref + widgets
        for c_name in {"Ref", "Widgets"}:
            if c_name in bpy.data.collections:
                bpy.data.collections.remove(bpy.data.collections[c_name])

        # TODO Auto clipping

        return {'FINISHED'}


class LFSSceneBuilderExportAEFile(bpy.types.Operator, ExportHelper):
    """Export current scene to After Effects"""
    bl_idname = "pipeline.scene_builder_export_ae"
    bl_label = "Export to AE"
    bl_options = {'REGISTER', 'UNDO'}

    filename_ext = ".jsx"
    filter_glob: bpy.props.StringProperty(default="*.jsx", options={'HIDDEN'})

    @classmethod
    def poll(cls, context):
        # From https://blenderartists.org/t/check-if-add-on-is-enabled-using-python/522226/2
        import addon_utils
        mod_name = "io_export_after_effects"
        is_enabled, is_loaded = addon_utils.check(mod_name)
        if not is_enabled:
            return False
        return bpy.ops.export.jsx.poll()

    def invoke(self, context, event):
        comp_dir = "//../../../compositing/"
        comp_dir_abs = os.path.abspath(bpy.path.abspath(comp_dir))
        self.directory = comp_dir

        if os.path.isdir(comp_dir_abs):
            versions = get_versions_from_filepath(os.path.join(comp_dir_abs, 'compositing_jsx'), extension='.jsx', search_from_version_file=False)
            if versions:
                self.filepath = versions[0]

        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        if context.mode != 'OBJECT':
            bpy.ops.object.mode_set(mode='OBJECT')
        start_frame = context.scene.frame_current
        context.scene.frame_set(context.scene.frame_end)

        # Select proper objects
        for obj in context.view_layer.objects:
            obj.select_set(False)

        # Add empties and parent them to rigs' position controllers
        for asset in context.scene.lfs_scene_builder_props.imported_assets:
            tracker_object_name = asset.name + '_null'
            if tracker_object_name in bpy.data.objects:
                tracker_object = bpy.data.objects.get(tracker_object_name)
                tracker_object.select_set(True)
                continue

            # Get rig object
            rig_obj = None
            if asset.proxy_object is not None:
                rig_obj = asset.proxy_object
            else:
                for obj_db in asset.object_datablocks:
                    if (obj_db.datablock.type == 'ARMATURE'
                            and ('_proxy' in obj_db.datablock.name
                                 or '_rig' in obj_db.datablock.name)):
                        rig_obj = obj_db.datablock
                        break

            # Create tracker object
            if rig_obj is not None and 'c_traj' in rig_obj.data.bones:
                tracker_object = bpy.data.objects.new(tracker_object_name, None)
                tracker_collection = bpy.data.collections.get("Trackers")
                if tracker_collection is None:
                    tracker_collection = bpy.data.collections.new("Trackers")
                tracker_collection.objects.link(tracker_object)
                if tracker_collection.name not in context.scene.collection.children:
                    context.scene.collection.children.link(tracker_collection)

                # Create constraint
                con = tracker_object.constraints.new('COPY_TRANSFORMS')
                con.target = rig_obj
                con.subtarget = 'c_traj'

            # Select tracker objects
            if "Trackers" in bpy.data.collections:
                tracker_collection = bpy.data.collections["Trackers"]
                for tracker_object in tracker_collection.objects:
                    tracker_object.select_set(True)

        if 'Sets' in bpy.data.collections:
            sets_coll = bpy.data.collections['Sets']
            # Ungroup planes
            for obj in sets_coll.all_objects[:]:
                if 'camera_plane_distance' in obj:
                    bpy.ops.camera.camera_plane_add_opacity(plane=obj.name)
                if "originals" in obj:
                    # Apply group scale and opacity animation on children when ungrouping
                    originals = obj['originals'][:]
                    for orig in originals:
                        if (obj.animation_data is not None
                                and obj.animation_data.action is not None):
                            orig.animation_data.action = obj.animation_data.action.copy()
                        orig['camera_plane_scale'] = obj['camera_plane_scale']
                        # Copy node group animation
                        orig_node_tree = orig.active_material.node_tree
                        obj_node_tree = obj.active_material.node_tree
                        bpy.ops.camera.camera_plane_add_opacity(plane=orig.name)
                        orig_node_tree.nodes["Opacity Multiplier"].inputs[1].default_value = obj_node_tree.nodes["Opacity Multiplier"].inputs[1].default_value
                        if (obj_node_tree.animation_data is not None
                                and obj_node_tree.animation_data.action is not None):
                            orig_node_tree.animation_data_create()
                            orig_node_tree.animation_data.action = obj_node_tree.animation_data.action.copy()

                    bpy.ops.camera.camera_plane_ungroup(group=obj.name)

            # Show sets
            sets_layer_coll = get_child_recursively('Sets', context.view_layer.layer_collection)
            if sets_layer_coll is not None:
                sets_coll.hide_select = False
                sets_coll.hide_viewport = False
                sets_layer_coll.hide_viewport = False
                sets_layer_coll.exclude = False
                for obj in sets_coll.all_objects[:]:
                    if obj.type == 'MESH':
                        obj.hide_select = False
                        obj.hide_set(False)
                        obj.hide_viewport = False
                        obj.select_set(True)

        # Export AE file
        bpy.ops.export.jsx(filepath=self.filepath)

        ## Ad hoc, don't try this at home

        addon_prefs = context.preferences.addons[__package__].preferences
        base_dir = addon_prefs.windows_comp_export_path or addon_prefs.root_path
        jsx_filepath = bpy.path.abspath(self.filepath)

        if addon_prefs.windows_comp_export_path:
            with open(jsx_filepath, 'r') as jsxf:
                content = jsxf.read()
                # Replace Unix paths with Windows ones if needed
                content = content.replace(addon_prefs.root_path.rstrip("/\\") + "/", base_dir.rstrip("/\\") + "/")
                # Forego prompt for comp name
                sub_pattern = r"""var compName = prompt\("Blender Comp's Name \\nEnter Name of newly created Composition","(.*)","Composition's Name"\);"""
                content = re.sub(sub_pattern, r'var compName = "\1";\n', content)
            with open(jsx_filepath, 'w') as jsxf:
                jsxf.write(content)

        # Create cmd script to run the AE script (!)
        cmd_path = jsx_filepath.replace(".jsx", ".bat")
        jsx_filepath = jsx_filepath.replace(addon_prefs.root_path.rstrip("/\\"), base_dir.rstrip("/\\"))
        jsx_filepath = jsx_filepath.replace('/', "\\")
        cmd_script = f""""C:\\Program Files\\Adobe\\Adobe After Effects 2020\\Support Files\\AfterFX.exe" -r {jsx_filepath}
"C:\\Program Files\\Adobe\\Adobe After Effects 2020\\Support Files\\AfterFX.exe" -r {base_dir}\\TECH\\scripts\\ae_import\\siren_setup_comp.jsx
"""
        with open(cmd_path, 'w') as f:
            f.write(cmd_script)

        context.scene.frame_set(start_frame)
        return {'FINISHED'}


class LFSSceneBuilderCreateCollections(bpy.types.Operator):
    """Create collections for assets"""
    bl_idname = "scene.setup_render_create_collections"
    bl_label = "Create Collections"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        render_coll = bpy.data.collections.get('RENDER')
        if render_coll is None:
            render_coll = bpy.data.collections.new('RENDER')
        if not render_coll.name in context.scene.collection.children:
            context.scene.collection.children.link(render_coll)

        all_coll = bpy.data.collections.get('All')
        if all_coll is None:
            all_coll = bpy.data.collections.new('All')
        if not all_coll.name in render_coll.children:
            render_coll.children.link(all_coll)

        # Convert all assets to overrides, to get unique object names
        convert_assets_to_overrides()

        collections_to_render = []

        for coll_name in RENDERED_COLLECTIONS:
            collection = bpy.data.collections.get(coll_name)
            if collection is not None:
                # Move collections inside the RENDER collection
                for child_coll in collection.children:
                    collections_to_render.append(child_coll)
                    # assign_collection_to_collection(child_coll, render_coll.name)

                # Move objects to the RENDER collection. Probably not needed...
                for obj in collection.objects[:]:
                    if (obj.proxy is not None and obj.proxy_collection is not None
                            and collection in obj.users_collection):
                        container = bpy.data.collections.new(obj.proxy_collection.name.rstrip("0123456789."))
                        collections_to_render.append(container)
                        # assign_collection_to_collection(container, render_coll.name)
                        assign_object_to_collection(obj, container.name)
                        assign_object_to_collection(obj.proxy_collection, container.name)

        # TODO Sort collections by Z

        # # Get collection centers
        # collection_centers = {}
        # for coll in collections_to_render:
        #     ...

        #     if arm_obj is not None:
        #         traj_loc = arm_obj.matrix_world @ arm_obj.pose.bones["c_traj"].matrix.to_translation()
        #         collection_centers[child_coll] = traj_loc

        # # Sort assets from left to right in camera view
        # colls = sorted(collection_centers, key=lambda coll: project_3d_point(cam, collection_centers[coll], bpy.context.scene.render).x)

        # Assign subcollections to the rendered layer
        for collection in collections_to_render:
            assign_collection_to_collection(collection, all_coll.name)

        sort_collections(context)

        # Assign main collections themselves (Chars, Props, etc.) to the rendered layer
        for coll_name in RENDERED_COLLECTIONS:
            collection = bpy.data.collections.get(coll_name)
            if collection is not None:
                if collection.all_objects:
                    assign_collection_to_collection(collection, all_coll.name)
                else:
                    for parent_coll in traverse_tree(context.scene.collection):
                        if collection.name in parent_coll.children:
                            parent_coll.children.unlink(collection)
        return {'FINISHED'}


class LFSSceneBuilderCreateViewLayers(bpy.types.Operator):
    """ Export assets renders on different layers """
    bl_idname = "scene.setup_render_layers"
    bl_label = "Create View Layers"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        # Keep only one view layer, rename it to 'View Layer' and reset holdouts
        reset_view_layers()

        render_coll = bpy.data.collections.get('RENDER')
        if render_coll is None:
            return {'CANCELLED'}

        i = 1  # For index in name of view layer
        for coll in render_coll.children:
            vl = bpy.context.scene.view_layers.new(f"{i:03}_{coll.name}".strip("_"))
            render_vl_collection = vl.layer_collection.children.get("RENDER")
            for other_coll in render_vl_collection.children:
                if other_coll.name != coll.name:
                    other_coll.exclude = True
                    for other_child_coll in traverse_tree(other_coll):
                        other_child_coll.holdout = True
            for other_coll in HIDDEN_COLLECTIONS:
                cl = vl.layer_collection.children.get(other_coll)
                if cl is not None:
                    cl.exclude = True
            i += 1

        return {'FINISHED'}


class LFSSceneBuilderCreateNodeTree(bpy.types.Operator):
    """ Create node tree to export renders on different layers """
    bl_idname = "scene.setup_render_node_tree"
    bl_label = "Create Node Tree"
    bl_options = {'REGISTER', 'UNDO'}

    directory: bpy.props.StringProperty(default="//../../../compositing/",
                                        maxlen=1024, subtype='DIR_PATH',
                                        options={'HIDDEN', 'SKIP_SAVE'})

    def invoke(self, context, event):
        comp_dir = "//../../../compositing/"
        comp_dir_abs = os.path.abspath(bpy.path.abspath(comp_dir))
        passes_dir = None
        self.directory = ''

        if os.path.isdir(comp_dir_abs):
            for sub in os.listdir(comp_dir_abs):
                if sub.endswith('_comp_passes'):
                    passes_dir = sub
                    break

        if passes_dir is not None:
            dirname = os.path.join(comp_dir_abs, passes_dir)
            versions = []
            for version in sorted(os.listdir(dirname), reverse=True):
                if re.match("^v[0-9]+$", version):
                    version = os.path.join(dirname, version)
                    versions.append(version)
            if versions:
                self.directory = versions[0]
            else:
                self.directory = dirname
        else:
            self.directory = comp_dir

        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        context.scene.render.engine = 'BLENDER_EEVEE'
        context.scene.eevee.taa_render_samples = 16
        context.scene.render.film_transparent = True
        context.scene.render.use_single_layer = False
        context.scene.render.resolution_x = 2048
        context.scene.render.resolution_y = 858
        context.scene.render.resolution_percentage = 100

        context.scene.render.image_settings.file_format = 'JPEG'
        context.scene.render.image_settings.color_mode = 'RGB'
        context.scene.render.image_settings.color_depth = '8'
        context.scene.render.image_settings.quality = 80
        context.scene.render.use_overwrite = False
        context.scene.render.use_placeholder = True

        # Set scene render path for placeholder
        self.directory = bpy.path.relpath(self.directory)
        context.scene.render.filepath = os.path.join(self.directory,
                                                     "_placeholder", "_placeholder.")

        create_node_tree(context, self.directory)

        return {'FINISHED'}


class PIPELINE_PT_lfs_scene_builder_comp_panel(bpy.types.Panel):
    bl_label = "Comp"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"
    bl_parent_id = "PIPELINE_PT_lfs_scene_builder_panel"

    @classmethod
    def poll(cls, context):
        addon_prefs = context.preferences.addons[__package__].preferences
        return addon_prefs.show_confo

    def draw(self, context):
        layout = self.layout

        camera = context.scene.camera
        found_camera = camera is not None
        if found_camera:
            use_camera_shift = camera.data.shift_x or camera.data.shift_y
        else:
            use_camera_shift = False

        if addon_utils.check("io_export_after_effects") == (True, True):
            layout.operator(LFSSceneBuilderExportAEFile.bl_idname, icon='EXPORT')
        else:
            row = layout.row()
            row.alignment = 'CENTER'
            row.label(text="Please activate After Effects export add-on", icon='ERROR')

        col = layout.column(align=True)
        col.operator(LFSSceneBuilderCreateCollections.bl_idname, icon='COLLECTION_NEW')
        col.operator(LFSSceneBuilderCreateViewLayers.bl_idname, icon='RENDERLAYERS')

        row = col.row(align=True)
        row.active = bpy.app.version >= (2, 93, 0)
        row.operator(LFSSceneBuilderCreateNodeTree.bl_idname, icon='NODETREE')

        if bpy.app.version < (2, 93, 0):
            layout.label(text="Please use Blender 2.93! The mask feature is unavailable otherwise.", icon='ERROR')

        if not found_camera:
            layout.label(text="No camera found in scene!", icon='ERROR')
        elif use_camera_shift:
            layout.label(text="Scene camera uses shift! Please replace it by a rotation and reexport.", icon='ERROR')

        if "kitsu_duration" in context.scene and context.scene["kitsu_duration"] != (context.scene.frame_end - context.scene.frame_start + 1):
            layout.label(text="Duration is different from Kitsu!", icon='ERROR')


classes = (LFSSceneBuilderSetupRender, LFSSceneBuilderCreateCollections,
           LFSSceneBuilderCreateViewLayers,
           LFSSceneBuilderCreateNodeTree,
           LFSSceneBuilderExportAEFile,
           PIPELINE_PT_lfs_scene_builder_comp_panel)


def register():
    for c in classes:
        bpy.utils.register_class(c)

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
